import Socket = NodeJS.Socket;

const mysql = require('mysql');

export default class MysqlDatabase {
  private connection: any;

  constructor(config: any) {
    this.connection = mysql.createConnection(config);
  }

  public connect() {
    return new Promise((resolve, reject) => {
      this.connection.connect(function (err: any) {
        if (err)
          return reject(err);
        resolve();
      });
    });
  }

  public query(sql: any, args?: any) {
    return new Promise<any>((resolve, reject) => {
      this.connection.query(sql, args, (err: any, rows: any) => {
        if (err) {
          console.log(err);
          return reject(err);
        }
        resolve(rows);
      });
    });
  }


  public close() {
    return new Promise((resolve, reject) => {
      this.connection.end((err: any) => {
        if (err)
          return reject(err);
        resolve();
      });
    }).finally();
  }

  public async insert(table: any, data: any) {
    let columnArr = [];
    let valueArr = [];
    for (let key in data) {
      if (!data.hasOwnProperty(key)) continue;
      columnArr.push(key);
      valueArr.push(data[key]);
    }
    let query = 'insert into ' + table + '(' + columnArr.join(',') + ') value (?)';
    return await this.query(query, [valueArr]);
  }

  public async update(table: string, data: any, where: string) {
    let columnArr = [];
    let valueArr = [];
    for (let key in data) {
      if (!data.hasOwnProperty(key)) continue;
      columnArr.push(key + '= ?');
      valueArr.push(data[key]);
    }
    let query = 'update ' + table + ' set ' + columnArr.join(',') + ' where ' + where;
    return await this.query(query, valueArr);
  }
}
