import Socket = NodeJS.Socket;

const mysql = require('mysql');

export default class MysqlDatabase {
  private connection;

  constructor(config) {
    this.connection = mysql.createConnection(config);
  }

  connect() {
    return new Promise((resolve, reject) => {
      this.connection.connect(function (err) {
        if (err)
          return reject(err);
        resolve();
      });
    });
  }

  query(sql, args) {
    return new Promise((resolve, reject) => {
      this.connection.query(sql, args, (err, rows) => {
        if (err) {
          console.log(err);
          return reject(err);
        }
        resolve(rows);
      });
    });
  }


  close() {
    return new Promise((resolve, reject) => {
      this.connection.end(err => {
        if (err)
          return reject(err);
        resolve();
      });
    });
  }

  async insert(table, data) {
    var columnArr = [];
    var valueArr = [];
    for (let key in data) {
      if (!data.hasOwnProperty(key)) continue;
      columnArr.push(key);
      valueArr.push(data[key]);
    }
    var query = 'insert into ' + table + '('+columnArr.join(',')+') value (?)';
    return await this.query(query, [valueArr]);
  }

  async update(table, data, where) {
    var columnArr = [];
    var valueArr = [];
    for (let key in data) {
      if (!data.hasOwnProperty(key)) continue;
      columnArr.push(key + '= ?');
      valueArr.push(data[key]);
    }
    var query = 'update ' + table + ' set ' + columnArr.join(',') + ' where ' + where;
    return await this.query(query, valueArr);
  }
}
