import {MongoClient} from 'mongodb';

export default class MongoDatabase {
  private db: MongoClient | undefined;
  private connectionString: string;
  private dbo: any;
  private options = {
    // userNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: 100
  };
  private config: any;

  constructor(config: any) {
    this.config = config;
    this.config.username = this.config.username == undefined ? '' : this.config.username;
    this.config.password = this.config.password == undefined ? '' : this.config.password;
    this.config.host = this.config.host == undefined ? 'localhost' : this.config.host;
    this.config.port = this.config.port == undefined ? '27017' : this.config.port;
    this.config.database = this.config.database == undefined ? '' : this.config.database;
    this.config.useSeedList = this.config.useSeedList == undefined ? '' : this.config.useSeedList;
    this.connectionString = this.rebuildConnectionString();
  }

  private rebuildConnectionString() {
    this.connectionString =
      'mongodb' + (this.config.useSeedList ? '+srv' : '') + '://' +
      this.config.username + ':' + this.config.password + '@' +
      this.config.host + (this.config.useSeedList ? '' : (':' + this.config.port)) +
      '/' + this.config.database;
    return this.connectionString;
  }

  public connect() {
    this.rebuildConnectionString();
    return new Promise<MongoDatabase>((resolve, reject) => {
      let self = this;
      MongoClient.connect(this.connectionString, this.options, function (err: any, db: any) {
        if (err) return reject(err);
        self.db = db;
        self.dbo = db.db(self.config.database);
        resolve(self);
      });
    });
  }

  public find(collection: any, query: any, options?: any) {
    return this.dbo.collection(collection).find(query, options);
  }

  public insert(collection: any, data: any) {
    if (data._id != undefined) {
      delete data._id;
    }
    return this.dbo.collection(collection).insertOne(data);
  }

  public insertMany(collection: any, data: any[]) {
    return this.dbo.collection(collection).insertMany(data);
  }

  public update(collection: any, data: any, filter: any) {
    if (data._id != undefined) {
      delete data._id;
    }
    return this.dbo.collection(collection).updateMany(filter, {'$set': data});
  }

  public aggregate(collection: any, aggregation: any) {
    return this.dbo.collection(collection).aggregate(aggregation, {cursor: { batchSize: 100000 }});
  }



  public close() {
    if (this.db != undefined) {
      this.db.close().finally();
    }
  }

  public static createConnection(config: any) {
    let conn = new MongoDatabase(config);
    return conn.connect();
  }
}
