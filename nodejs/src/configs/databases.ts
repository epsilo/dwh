require('dotenv').config();
export default {
  mysql: {
    epsilo_m: {
      host: process.env.DB_EPSILO_M_HOST,
      user: process.env.DB_EPSILO_M_USERNAME,
      password: process.env.DB_EPSILO_M_PASSWORD,
      database: process.env.DB_EPSILO_M_DATABASE
    }
  },
  mongo: {
    crawler_log: {
      host: process.env.MONGO_LOG_CRAWLER_HOST,
      port: parseInt(process.env.MONGO_LOG_CRAWLER_PORT),
      username: process.env.MONGO_LOG_CRAWLER_USERNAME,
      password: process.env.MONGO_LOG_CRAWLER_PASSWORD,
      database: process.env.MONGO_LOG_CRAWLER_DATABASE,
      useSeedList: JSON.parse(process.env.MONGO_LOG_CRAWLER_SRV),
    }
  }
};
