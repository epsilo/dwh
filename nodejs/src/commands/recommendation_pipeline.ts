import {Command, flags} from '@oclif/command'
import MongoDatabase from "../libs/MongoDatabase";
import MysqlDatabase from "../libs/MysqlDatabase";
import databasesConfigs from '../configs/databases';
import databases from "../configs/databases";

export default class RecommendationPipeline extends Command {
  static description = 'DATA Pipeline for EPS_M Recommendation';

  static examples = [];

  static flags = {
    help: flags.help({char: 'h'}),
    'shop-id': flags.string({char: 's', description: 'shop ID', required: true}),
    keyword: flags.string({char: 'k', description: 'Keyword name EX: ahc', required: true}),
    sku: flags.string({char: 'u', description: 'Sku', required: true}),
    from: flags.string({char: 'f', description: 'From unix time', required: true}),
    debug: flags.boolean({char: 'd', description: 'Debug flag'}),
  };

  static args = [{name: 'file'}];
  isDebug = false;
  scriptBeginTime = 0;

  async run() {
    this.scriptBeginTime = (new Date()).getTime();
    /* Prepare parameter -------------------------------------------------------------------------------------------- */
    const {args, flags} = this.parse(RecommendationPipeline);
    let startTime = flags.from == undefined ? 0 : (parseInt(flags.from.toString()) - 60);
    let sku = flags.sku;
    let keyword = flags.keyword;
    let shopId = flags['shop-id'];
    this.isDebug = flags.debug;

    /* Initialize Database Connection ------------------------------------------------------------------------------- */
    const mysqlMConn = new MysqlDatabase(databasesConfigs.mysql.epsilo_m);
    const mongoConn = await MongoDatabase.createConnection(databasesConfigs.mongo.crawler_log);

    /* Preparing data for processing -------------------------------------------------------------------------------- */
    let masterData = await mysqlMConn.query(RecommendationPipeline.buildQueryRank(startTime, keyword, sku, shopId));
    this.printTime();
    let maxTime = parseInt(Math.max.apply(Math, masterData.map(function (o: any) {
      return o.timestamp;
    })).toString());
    let rtbDataRaw = await mongoConn.find('dataline', {
      keyword: keyword,
      sku: sku,
      timestamp: {$gte: startTime, $lte: maxTime}
    }).toArray();
    this.printTime();
    let rtbData: any = {};
    rtbData['list'] = {};
    rtbData['time'] = [];
    for (let i = 0; i < rtbDataRaw.length; i++) {
      rtbData['list'][rtbDataRaw[i].timestamp] = rtbDataRaw[i].price;
      rtbData['time'].push(rtbDataRaw[i].timestamp);
    }
    let topDataQuery = RecommendationPipeline.topSkuQuery(keyword, startTime, maxTime);
    this.log(JSON.stringify(topDataQuery));
    let topData = await mongoConn.aggregate('product_ranking_shopee', topDataQuery).toArray();
    this.printTime();
    let topSkuList = [];
    for (let k = 0; k < topData.length; k++) {
      topSkuList.push(parseInt(topData[k]._id.sku));
    }
    let skuQueryList = RecommendationPipeline.topSkuPosition(keyword, startTime, maxTime, topSkuList);
    this.log(JSON.stringify(skuQueryList));
    let skuDataList = await mongoConn.aggregate('product_ranking_shopee', skuQueryList).toArray();
    this.printTime();
    let assocData = {};
    for (let l = 0; l < skuDataList.length; l++) {
      let itemSku = skuDataList[l].itemid;
      assocData[itemSku] = assocData[itemSku] || [];
      assocData[itemSku].push(skuDataList[l]);
    }
    for (let k = 0; k < topData.length; k++) {
      let itemSku = topData[k]._id.sku;
      topData[k]['pos_list'] = {};
      topData[k]['pos_time'] = [];
      this.log('initialize: ' + (k + 1) + ' COUNT: ' + assocData[itemSku].length);
      for (let i = 0; i < assocData[itemSku].length; i++) {
        topData[k]['pos_list'][assocData[itemSku][i].timeRaw] = assocData[itemSku][i].isSponsorPosition;
        topData[k]['pos_time'].push(assocData[itemSku][i].timeRaw);
      }
      topData[k]['pos_time'].sort();
      this.printTime();
    }
    /* Processing Data for exporting -------------------------------------------------------------------------------- */
    for (let k = 0; k < masterData.length; k++) {
      let data = masterData[k];
      data.position = parseInt(data.position);
      data.position = isNaN(data.position) || data.position == 0 ? null : data.position;
      let filter:any = {};
      if (data.position != null) {
        filter[data.position] = [{
          index: null,
          timestamp: data.timestamp
        }];
      }
      let rtbTimeIndex = this.binarySearchSmaller(rtbData['time'], data.timestamp, 0, rtbData['time'].length - 1);
      data.price = rtbData['list'][rtbData['time'][rtbTimeIndex]] || null;
      for (let i = 1; i <= 10; i++) {
        if (topData[i - 1] != undefined) {
          let timeIndex = this.binarySearchSmaller(topData[i - 1]['pos_time'], data.timestamp, 0, topData[i - 1]['pos_time'].length - 1);
          data['sku_no_' + i] = topData[i - 1]['pos_list'][topData[i - 1]['pos_time'][timeIndex]] || null;
          if (data['sku_no_' + i] != null) {
            if (filter[data['sku_no_' + i]] == undefined) {
              filter[data['sku_no_' + i]] = [];
            }
            filter[data['sku_no_' + i]].push({
              index: i,
              timestamp: topData[i - 1]['pos_time'][timeIndex]
            });
          }
        }
      }
      // /*/
      for (let keyItem in filter) {
        if (!filter.hasOwnProperty(keyItem)) continue;
        if (filter[keyItem].length > 1) {
          let maxI = 0;
          for (let j = 1; j < filter[keyItem].length; j++) {
            if (filter[keyItem][maxI].timestamp < filter[keyItem][j].timestamp) {
              maxI = j;
            }
          }
          for (let j = 0; j < filter[keyItem].length; j++) {
            if (j != maxI) {
              if (filter[keyItem][j].index != null) {
                data['sku_no_' + filter[keyItem][j].index] = null;
              }
            }
          }
        }
      }//*/
      let str = JSON.stringify([data.keyword,data.sku,this.dateConvert(data.timestamp),data.position,data.price,data.sku_no_1,data.sku_no_2,data.sku_no_3,data.sku_no_4,data.sku_no_5,data.sku_no_6,data.sku_no_7,data.sku_no_8,data.sku_no_9,data.sku_no_10]);
      let newStr = str.slice(1, str.length -1); // .replace(/null/g,'');
      console.log(newStr);
    }
    this.printTime();
    /* End Database Connection -------------------------------------------------------------------------------------- */
    mongoConn.close();
    mysqlMConn.close();
  }

  private static topSkuPosition(keyword: any, startTime: any, maxTime: any, skuList: any[]) {
    return [
      {$match: {timeRaw: {$gte: startTime, $lte: maxTime}}},
      {
        $match: {
          isSponsorPosition: {$ne: null},
          keywordSearch: keyword,
          itemid: {$in: skuList}
        }
      },
      {$project: {timeRaw: 1, isSponsorPosition: 1, itemid: 1}}
    ];
  }

  private static topSkuQuery(keyword: any, startTime: any, maxTime: any) {
    return [{
      $match: {
        isSponsorPosition: {$gt: 0},
        keywordSearch: keyword,
        timeRaw: {$gte: startTime, $lte: maxTime},
      }
    }, {
      $group: {
        _id: {sku: '$itemid', keyword: '$keywordSearch',},
        avg: {$avg: '$isSponsorPosition'}
      }
    }, {$sort: {'_id.keyword': 1, 'avg': 1}}, {$limit: 10}];
  }

  private static buildQueryRank(time: any, keyword: any, sku: any, shopId: any) {
    time = parseInt(time).toString();
    keyword = keyword.toString();
    sku = sku.toString();
    shopId = parseInt(shopId).toString();
    return '' +
      'select product_shop_channel_item_id sku,\n' +
      '       shop_channel_id scid,\n' +
      '       mak_programmatic_keyword_name keyword,\n' +
      '       mak_craw_ranking_collect_at timestamp,\n' +
      '       mak_craw_ranking_rank position\n' +
      'from mak_craw_ranking\n' +
      'join product_shop_channel on mak_craw_ranking.fk_product_shop_channel = product_shop_channel.product_shop_channel_id\n' +
      'join shop_channel on product_shop_channel.fk_shop_channel = shop_channel.shop_channel_id\n' +
      'join mak_programmatic_keyword on mak_craw_ranking.fk_mak_programmatic_keyword = mak_programmatic_keyword.mak_programmatic_keyword_id\n' +
      'where  mak_craw_ranking_collect_at >= ' + time + '\n' +
      'and mak_programmatic_keyword_name = \'' + keyword + '\'\n' +
      'and product_shop_channel_item_id = \'' + sku + '\'\n' +
      'and fk_shop_master = ' + shopId;
  }

  printTime() {
    let now = (new Date()).getTime();
    this.lastRun = this.lastRun == 0 ? this.scriptBeginTime : this.lastRun;
    this.log(
      'RUNNING: ' + (now - this.lastRun ).toString() + 'ms' +
      ' ' +
      'TOTAL: ' + (now - this.scriptBeginTime).toString() + 'ms'
    );
    this.lastRun  = now;
  }

  lastRun = 0;

  log(message?: any, ...args: any[]): void {
    if (this.isDebug) {
      super.log(message, ...args);
    }
  }

  // @ts-ignore
  binarySearchSmaller(array: any[], value: any, i: any, j: any) {
    if (array.length == 0) return -1;
    let x = Math.floor((i + j) / 2);
    let y = x + 1;
    if (array[x] <= value && array[y] >= value) {
      return x;
    }
    if (x == i && y == j) {
      if (array[y] <= value) {
        return y;
      }
      return -1;
    }
    if (array[x] >= value) {
      return this.binarySearchSmaller(array, value, i, x);
    } else {
      return this.binarySearchSmaller(array, value, x, j);
    }
  }

  dateConvert(input: any) {

    let date = new Date(input * 1000);
    let year = date.getFullYear();
    let month = "0" + (date.getMonth() + 1);
    let day = "0" + date.getDate();
    let hours = "0" + date.getHours();
    let minutes = "0" + date.getMinutes();
    let seconds = "0" + date.getSeconds();
    return year + '-' + month.substr(-2) + '-' + day.substr(-2) + ' ' +
      hours.substr(-2) + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  }
}
