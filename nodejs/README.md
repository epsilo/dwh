data_pipeline
=============



[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/data_pipeline.svg)](https://npmjs.org/package/data_pipeline)
[![Downloads/week](https://img.shields.io/npm/dw/data_pipeline.svg)](https://npmjs.org/package/data_pipeline)
[![License](https://img.shields.io/npm/l/data_pipeline.svg)](https://github.com/epsilo/data_pipeline/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g data_pipeline
$ data_pipeline COMMAND
running command...
$ data_pipeline (-v|--version|version)
data_pipeline/0.0.0 linux-x64 node-v10.15.2
$ data_pipeline --help [COMMAND]
USAGE
  $ data_pipeline COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`data_pipeline hello [FILE]`](#data_pipeline-hello-file)
* [`data_pipeline help [COMMAND]`](#data_pipeline-help-command)

## `data_pipeline hello [FILE]`

describe the command here

```
USAGE
  $ data_pipeline hello [FILE]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print

EXAMPLE
  $ data_pipeline hello
  hello world from ./src/hello.ts!
```

_See code: [src/commands/hello.ts](https://github.com/epsilo/data_pipeline/blob/v0.0.0/src/commands/hello.ts)_

## `data_pipeline help [COMMAND]`

display help for data_pipeline

```
USAGE
  $ data_pipeline help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.1/src/commands/help.ts)_
<!-- commandsstop -->
