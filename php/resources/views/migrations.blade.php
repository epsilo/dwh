<?php echo "
<?php
/**
 * Warning: This class is generated automatically by schema_update. !!! Do not touch or modify
 *
 * Created by HAOU SHIN.
 * User: HaouShin
 * Date: {$date}
 * Time: {$hour}
 */
namespace App\Library\Model\Sql\\{$namespace};

use App\Library\Model\SqlModel;


class {$className} extends SqlModel
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected \$connection = '{$connection}';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected \$table = '{$tableName}';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected \$primaryKey = '{$primaryKey}';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public \$incrementing = {$incrementing};

    {$constants}

    /**
     * @const string
     */
    const _tableName = '{$tableName}';

    /**
     * @const string
     */
    const CONNECTION_NAME = '{$connection}';
}";