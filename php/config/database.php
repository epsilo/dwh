<?php
$cfg = [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'default'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */
    'connections' => [
        'default' => [
            'options'   => [PDO::ATTR_EMULATE_PREPARES => env('PDO_EMULATE_PREPARES', true)],
            'driver' => 'mysql',
            'write' => [
                'host' => env('DB_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_WRITE_USERNAME', 'remote'),
                'password' => env('DB_WRITE_PASSWORD', 'Remote@123456'),
                'database' => env('DB_WRITE_DATABASE', 'data'),
            ],
            'read' => [
                'host' => env('DB_READ_HOST', '127.0.0.1'),
                'username' => env('DB_READ_USERNAME', 'remote'),
                'password' => env('DB_READ_PASSWORD', 'Remote@123456'),
                'database' => env('DB_READ_DATABASE', 'data'),
            ],
            'port' => 3306,
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
        ],
        'kernel' => [
            'options'   => [PDO::ATTR_EMULATE_PREPARES => env('PDO_EMULATE_PREPARES', true)],
            'driver' => 'mysql',
            'write' => [
                'host' => env('DB_KERNEL_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_KERNEL_WRITE_USERNAME', 'remote'),
                'password' => env('DB_KERNEL_WRITE_PASSWORD', 'Remote@123456'),
                'database' => env('DB_KERNEL_WRITE_DATABASE', 'kernel'),
            ],
            'read' => [
                'host' => env('DB_KERNEL_READ_HOST', '127.0.0.1'),
                'username' => env('DB_KERNEL_READ_USERNAME', 'remote'),
                'password' => env('DB_KERNEL_READ_PASSWORD', 'Remote@123456'),
                'database' => env('DB_KERNEL_READ_DATABASE', 'kernel'),
            ],
            'port' => 3306,
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
        ],
        'data_warehouse' => [
            'options'   => [PDO::ATTR_EMULATE_PREPARES => env('PDO_EMULATE_PREPARES', true)],
            'driver' => 'mysql',
            'write' => [
                'host' => env('DB_DATA_WAREHOUSE_WRITE_HOST', '127.0.0.1'),
                'username' => env('DB_DATA_WAREHOUSE_WRITE_USERNAME', 'remote'),
                'password' => env('DB_DATA_WAREHOUSE_WRITE_PASSWORD', 'Remote@123456'),
                'database' => env('DB_DATA_WAREHOUSE_WRITE_DATABASE', 'data'),
            ],
            'read' => [
                'host' => env('DB_DATA_WAREHOUSE_READ_HOST', '127.0.0.1'),
                'username' => env('DB_DATA_WAREHOUSE_READ_USERNAME', 'remote'),
                'password' => env('DB_DATA_WAREHOUSE_READ_PASSWORD', 'Remote@123456'),
                'database' => env('DB_DATA_WAREHOUSE_READ_DATABASE', 'data'),
            ],
            'port' => 3306,
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',
    'redis' => [
        'client' => 'predis',
        'cluster' => env('REDIS_CLUSTER', false),
        'default' => [
            'host'     => env('REDIS_HOST', '127.0.0.1'),
            'port'     => env('REDIS_PORT', 6379),
            'database' => env('REDIS_DATABASE', 0),
        ],
        'cache' => [
            'host'     => env('CACHE_REDIS_HOST', '127.0.0.1'),
            'port'     => env('CACHE_REDIS_PORT', 6379),
            'database' => env('CACHE_REDIS_DATABASE', 0),
        ],
        'queue' => [
            'host'     => env('QUEUE_REDIS_HOST', '127.0.0.1'),
            'port'     => env('QUEUE_REDIS_PORT', 6379),
            'database' => env('QUEUE_REDIS_DATABASE', 0),
        ],
        'session' => [
            'host'     => env('SESSION_REDIS_HOST', '127.0.0.1'),
            'port'     => env('SESSION_REDIS_PORT', 6379),
            'database' => env('SESSION_REDIS_DATABASE', 0),
        ],
    ],

    'mongodb' => [
        'data_forest' => [
            'host'     => env('MONGO_DATA_FOREST_HOST', 'localhost'),
            'port'     => env('MONGO_DATA_FOREST_PORT', 27017),
            'database' => env('MONGO_DATA_FOREST_DATABASE', 'epsilo'),
            'username' => env('MONGO_DATA_FOREST_USERNAME'),
            'password' => env('MONGO_DATA_FOREST_PASSWORD'),
        ],
    ],

    'elastic_search' => [
        'staging_area' => [
            'scheme'     => env('ES_STAGING_AREA_SCHEME', 'http'),
            'host'     => env('ES_STAGING_AREA_HOST', 'localhost'),
            'port'     => env('ES_STAGING_AREA_PORT', 9200),
        ],
    ],
];
if (env('REDIS_PASSWORD', null)) {
    $cfg['redis']['default']['password'] = env('REDIS_PASSWORD', null);
    $cfg['redis']['cache']['password'] = env('CACHE_REDIS_PASSWORD', null);
    $cfg['redis']['queue']['password'] = env('QUEUE_REDIS_PASSWORD', null);
    $cfg['redis']['session']['password'] = env('SESSION_REDIS_PASSWORD', null);
}
return $cfg;