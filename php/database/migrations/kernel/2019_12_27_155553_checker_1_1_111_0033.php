<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Checker111110033 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('kernel')->create('checker_1_1_111_0033', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_id');
            $table->unsignedInteger('channel_id');
            $table->string('identify');
            $table->unsignedInteger('created_at');
            $table->index([
                'shop_id',
                'channel_id',
                'identify'
            ], 'checker_1_1_111_0033_identifier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('kernel')->drop('checker_1_1_111_0033');
    }
}
