<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Hourly111110033 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('data_warehouse')->create('hourly_1_1_111_0033', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('value', 16,4);
            $table->string('time', 20);
            $table->unsignedInteger('shop_id');
            $table->unsignedInteger('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('data_warehouse')->drop('hourly_1_1_111_0033');
    }
}
