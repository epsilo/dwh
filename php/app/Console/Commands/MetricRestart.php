<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 01/10/2019
 * Time: 14:18
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;


class MetricRestart extends Command
{
    protected $signature = 'metric-restart {--c|code=}';

    protected $description = 'ReStart metric';

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $metricCode = $this->option('code');
        if ($metricCode) {
            Cache::forever('RESET_METRICS:metric_'.$metricCode, time());
        } else {
            Cache::forever('RESET_METRICS:ALL', time());
        }
        $this->line('Broadcast reset signal!');
    }
}