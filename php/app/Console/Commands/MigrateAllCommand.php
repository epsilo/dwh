<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;


class MigrateAllCommand extends Command
{
    protected $signature = 'migrate:all {--g|generate=}';

    protected $description = 'Migrate All';

    public function handle()
    {
        Cache::clear();
        $this->info(str_repeat('=', 75) . '#');
        $name = 'data_warehouse';
        $this->info('Running migration for "' . $name . '"');
        $this->call('migrate', array('--database' => $name, '--path' => 'database/migrations/' . $name));

        $this->info(str_repeat('=', 75) . '#');
        $name = 'kernel';
        $this->info('Running migration for "' . $name . '"');
        $this->call('migrate', array('--database' => $name, '--path' => 'database/migrations/' . $name));

        if ($this->option('generate')) $this->call('migrate:generate');
        Cache::clear();
    }
}