<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Library\Application\Functions;

class MigrateGenerateCommand extends Command
{
    protected $signature = 'migrate:generate';

    protected $description = 'Migrate Model Generation';

    public function handle()
    {
        $this->info(str_repeat('=', 75) . '#');
        $name = 'data_warehouse';
        $path = SYS_PATH . '/app/Library/Model/Sql/' . self::snakeToPascal($name);
        $this->info('Generate Model for "' . $name . '"');
        self::generateModel(DB::connection($name), $path, self::snakeToPascal($name));

        $this->info(str_repeat('=', 75) . '#');
        $name = 'kernel';
        $path = SYS_PATH . '/app/Library/Model/Sql/' . self::snakeToPascal($name);
        $this->info('Generate Model for "' . $name . '"');
        self::generateModel(DB::connection($name), $path, self::snakeToPascal($name));

    }

    private static function generateModel($connection, $path, $namespace)
    {
        self::removePath($path);
        mkdir($path);
        $allTable = $connection->select('SHOW TABLES');
        foreach ($allTable as $table) {
            foreach ($table as $key => $tableName) {
                $schema = $connection->select("DESCRIBE `$tableName`");
                $idColumn = '';
                $autoIncrement = false;
                $constants = "";
                foreach ($schema as $column) {
                    if ($column->Key == 'PRI') {
                        $idColumn = $column->Field;
                        $autoIncrement = is_int(strpos($column->Extra,'auto_increment'));
                    }
                    $constKey = 'COL_'.strtoupper($column->Field);
                    $constants .= "/**
     * @var string
     */
    const {$constKey} = '{$column->Field}';
    
    ";
                }
                $view = view('migrations');
                $view->date = Functions\Convert::secondToDate(time());
                $view->hour = date('H:i', time());
                $view->className = self::snakeToPascal($tableName);
                $view->connection = $connection->getName();
                $view->tableName = $tableName;
                $view->primaryKey = $idColumn;
                $view->namespace = $namespace;
                $view->incrementing = $autoIncrement ? 'true' : 'false';
                $view->constants = $constants;
                $content = $view->render();
                file_put_contents($path.'/'.$view->className.'.php', $content);
            }
        }
    }

    private static function snakeToPascal($str)
    {
        return str_replace('_', '', ucwords($str, '_'));
    }

    /**
     * Remove folder
     * @param string $path
     */
    private static function removePath($path)
    {
        if (file_exists($path)) {
            if (is_dir($path)) {
                $arrDir = scandir($path);
                foreach ($arrDir as $dir) {
                    if (in_array($dir, array('.', '..'))) {
                        continue;
                    }
                    self::removePath($path . '/' . $dir);
                }
                rmdir($path);
            } else {
                unlink($path);
            }
        }
    }
}