<?php

namespace App\Console\Commands\Metrics\metric_1_1_111_0033;

use App\Library\Application\Functions\Convert;
use App\Library\Application\Functions\Timezone;
use App\Model\ES\StagingArea;
use App\Model\Sql\DataWarehouse;
use App\Model\Sql\Kernel\Checker111110033;
use Exception;
use http\Message\Body;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class KernelSaleorderitemShopeeApi extends Command
{


    protected $signature = 'metric:1_1_111_0033_kernel_saleorderitem_shopee_api {--f|from=} {--d|debug}';
    protected $description = 'Metric <fg=green>1_1_111_0033</fg=green> with datasource <fg=red>kernel_saleorderitem_Shopee_api</fg=red> PIPELINE';
    private $startTime = 0;

    public function __construct()
    {
        parent::__construct();
        $this->startTime = time();
    }

    private function checkReset()
    {
        $thisShortClassName = substr(strrchr(__CLASS__, "\\"), 1);
        $resetThisMetric = Cache::get('RESET_METRICS:' . $thisShortClassName, 0);
        $resetAllMetric = Cache::get('RESET_METRICS:ALL', 0);
        return $this->startTime <= $resetAllMetric || $this->startTime <= $resetAllMetric;
    }

    /**
     * @throws Exception
     */
    public function handle()
    {
        $from = $this->option('from') ?? 0;
        $debug = $this->option('debug');
        $count = 0;
        $countNone = 0;
        $limit = 10000;
        $model = new StagingArea\KernelSaleorderitemShopeeApi();
        while (true) {
            $start = $from;
            $from = time();
            $count++;
            $check = 0;
            $response = $model->search([
                'scroll' => '30s',
                'size' => $limit,
                'body' => [
                    'query' => [
                        'range' => ['createdAt' => ['gte' => $start]]
                    ]
                ],
            ], $debug);
            while (isset($response['hits']['hits']) && count($response['hits']['hits']) > 0) {
                $check = 1;
                foreach ($response['hits']['hits'] as $item) {
                    $this->line($item['_id'] . ' - Processing...', 'fg=yellow');
                    $this->processingItem($item);
                    $this->line('Processed', 'fg=green');
                }
                $scrollId = $response['_scroll_id'];
                $response = $model->scroll($scrollId, '30s');
            }
            $countNone = $check ? 0 : ($countNone + 1);
            if ($this->checkReset()) goto end;
            $sleepTime = $countNone == 0 ? 0 : ($countNone < 3 ? 1 : 3);
            sleep($sleepTime);
        }

        end:
        $this->line('Stop pipeline gracefully!', 'fg=green');
    }

    private function processingItem($elasticItem)
    {
        $data = $elasticItem['_source'];
        $shopId = $data['info']['shopId'];
        $channelId = $data['info']['channelId'];
        foreach ($data['data']['orders'] as $order) {
            foreach ($order['items'] as $item) {
                $orderId = $order['ordersn'];
                $orderItemId = $item['item_id'];
                $identify = $orderId . '-' . $orderItemId;
                $checker = Checker111110033::getByKeyIndex($shopId, $channelId, $identify);
                if ($checker) continue;
                $checker = new Checker111110033();
                $checker->{Checker111110033::COL_SHOP_ID} = $shopId;
                $checker->{Checker111110033::COL_CHANNEL_ID} = $channelId;
                $checker->{Checker111110033::COL_IDENTIFY} = $identify;
                $checker->{Checker111110033::COL_CREATED_AT} = time();
                Timezone::setByVentureId(Timezone::HO_CHI_MINH_TIMEZONE);

                $createdAt = Convert::secondToDateTime($order['create_time']);
                $unitItemValue = $item['variation_discounted_price'];
                $unitItemValue = $unitItemValue ? $unitItemValue : $item['variation_original_price'];
                $itemValue = $item['variation_quantity_purchased'] * $unitItemValue;

                $this->hourly($createdAt, $shopId, $itemValue);
                $this->daily($createdAt, $shopId, $itemValue);
                $this->weekly($createdAt, $shopId, $itemValue);
                $this->monthly($createdAt, $shopId, $itemValue);

                $checker->save();
            }
        }
    }

    private function hourly($createdAt, $shopId, $itemValue)
    {
        $time = date('Y-m-d H:00', strtotime($createdAt));
        $row = DataWarehouse\Hourly111110033::getByKey($shopId, $time) ?? new DataWarehouse\Hourly111110033();
        $row->{DataWarehouse\Hourly111110033::COL_VALUE} += $itemValue;
        $row->{DataWarehouse\Hourly111110033::COL_TIME} = $time;
        $row->{DataWarehouse\Hourly111110033::COL_SHOP_ID} = $shopId;
        $row->{DataWarehouse\Hourly111110033::COL_CREATED_AT} = $row->{DataWarehouse\Hourly111110033::COL_CREATED_AT} ?? time();
        $row->save();
    }

    private function daily($createdAt, $shopId, $itemValue)
    {
        $time = date('Y-m-d', strtotime($createdAt));
        $row = DataWarehouse\Daily111110033::getByKey($shopId, $time) ?? new DataWarehouse\Daily111110033();
        $row->{DataWarehouse\Daily111110033::COL_VALUE} += $itemValue;
        $row->{DataWarehouse\Daily111110033::COL_TIME} = $time;
        $row->{DataWarehouse\Daily111110033::COL_SHOP_ID} = $shopId;
        $row->{DataWarehouse\Daily111110033::COL_CREATED_AT} = $row->{DataWarehouse\Daily111110033::COL_CREATED_AT} ?? time();
        $row->save();
    }

    private function weekly($createdAt, $shopId, $itemValue)
    {
        $timestamp = strtotime($createdAt);
        $time = date('Y-', $timestamp) . intval(date('W', $timestamp));
        $row = DataWarehouse\Weekly111110033::getByKey($shopId, $time) ?? new DataWarehouse\Weekly111110033();
        $row->{DataWarehouse\Weekly111110033::COL_VALUE} += $itemValue;
        $row->{DataWarehouse\Weekly111110033::COL_TIME} = $time;
        $row->{DataWarehouse\Weekly111110033::COL_SHOP_ID} = $shopId;
        $row->{DataWarehouse\Weekly111110033::COL_CREATED_AT} = $row->{DataWarehouse\Weekly111110033::COL_CREATED_AT} ?? time();
        $row->save();
    }

    private function monthly($createdAt, $shopId, $itemValue)
    {
        $time = date('Y-m', strtotime($createdAt));
        $row = DataWarehouse\Monthly111110033::getByKey($shopId, $time) ?? new DataWarehouse\Monthly111110033();
        $row->{DataWarehouse\Monthly111110033::COL_VALUE} += $itemValue;
        $row->{DataWarehouse\Monthly111110033::COL_TIME} = $time;
        $row->{DataWarehouse\Monthly111110033::COL_SHOP_ID} = $shopId;
        $row->{DataWarehouse\Monthly111110033::COL_CREATED_AT} = $row->{DataWarehouse\Monthly111110033::COL_CREATED_AT} ?? time();
        $row->save();
    }
}