<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Application;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use SebastianBergmann\CodeCoverage\Report\PHP;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->commands = $this->link(SYS_PATH.'/app/Console/Commands/');
    }

    private function link($path)
    {
        $result = [];
        foreach (scandir($path) as $file) {
            if (in_array($file,['.','..'])) continue;
            if (is_dir($path. $file)) {
                $result = array_merge($result, $this->link($path . $file . '/'));
            } else {
                if (intval(strpos($file, '.php')) < 1) continue;
                $result[] =
                str_replace(
                    '/', '\\', str_replace(SYS_PATH.'/app/Console/Commands/', 'App\\Console\\Commands\\', $path)
                )
                . str_replace('.php', '', $file);
            }
        }
        return $result;
    }

    protected $commands = [];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
