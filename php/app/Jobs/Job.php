<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

abstract class Job implements ShouldQueue
{
    const QUEUE_BI = 'bi';
    const QUEUE_CLIENT = 'client_bi';
    const QUEUE_CLIENT_ITEM = 'client_item';

    const QUEUE_CRAWLER_PRICE_SCRAPER = 'crawler_price_scrapping';
    const QUEUE_LAZADA_CHAT_RESPONSE = 'lazada_chat_response';
    const QUEUE_LAZADA_ORDER_REVIEWS = 'lazada_order_reviews';
    const QUEUE_LAZADA_PRODUCT_ATP = 'lazada_product_atp';
    const QUEUE_LAZADA_PRODUCT_CONTENT = 'lazada_product_content';

    /**
     * @var bool
     */
    private static $debug = false;

    /**
     * @var int
     */
    private $startTime;

    /**
     * Debug Mode - Print Process
     * @param bool $debug
     */
    public static function debugMode($debug = true) {
        self::$debug = $debug;
    }

    /**
     * PRINT DATA
     * @param $data
     */
    public static function printDebug($data) {
        if (self::$debug) {
            print_r($data);
        }
    }

    /**
     * @return boolean
     */
    protected static function getDebugStatus()
    {
        return self::$debug;
    }

    /**
     * PRINT DATA
     * @param $data
     */
    protected function printData($data = null) {
        self::printDebug($data);
    }

    /**
     * Start Timer
     */
    protected function startTimer() {
        $this->startTime = round(microtime(true) * 1000);
    }

    /**
     * @return float
     */
    protected function currentTime() {
        return round(microtime(true) * 1000) - $this->startTime;
    }

    /**
     * Print Executed Time
     */
    protected function printExecutedTime()
    {
        $this->printData(PHP_EOL.'EXECUTED_TIME: ' . $this->currentTime() . 'ms' . PHP_EOL);
    }

    /**
     * Print Memory Usage
     */
    protected function printMemoryUsage()
    {
        $this->printData(number_format((memory_get_usage() / (1024*1024)),2).'MB');
    }

    /**
     * @return int
     */
    protected function getStartTimeSecond()
    {
        return intval($this->startTime / 1000);
    }

    abstract public function handle();

    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "queueOn" and "delay" queue helper methods.
    |
    */

    use InteractsWithQueue, Queueable, SerializesModels;
}
