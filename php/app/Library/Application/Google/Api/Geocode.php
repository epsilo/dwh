<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 15:10
 */
namespace App\Library\Application\Google\Api;

class Geocode
{
    private $urlGoogle;

    public function getUrlGoogle(){
        return $this->urlGoogle = "https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=AIzaSyC4FrJCplvpBXPwE3s0teRF-2jQbP4tIhE";
    }

    function getGeoLocation($addr)
    {
        $cleanAddress = str_replace (" ", "+", $addr);
        $details_url = sprintf($this->getUrlGoogle(),$cleanAddress);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $details_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $geoloc = json_decode(curl_exec($ch), true);
        return $geoloc;
    }
}