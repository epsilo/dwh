<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 24/10/2019
 * Time: 13:48
 */

namespace App\Library\Application\Constants;


interface Country
{
    const VIETNAM = 1;

    const PHILIPPINES = 2;

    const MALAYSIA = 3;

    const SINGAPORE = 4;

    const INDONESIA = 5;

    const THAILAND = 6;
}