<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 15/09/2019
 * Time: 11:03
 */

namespace App\Library\Application\Functions;


class Convert
{
    public static function stdClassToArray($stdClass)
    {
        if (is_object($stdClass)) $stdClass = (array)$stdClass;
        if (is_array($stdClass)) {
            $new = array();
            foreach ($stdClass as $key => $val) {
                $new[$key] = self::stdClassToArray($val);
            }
        } else $new = $stdClass;
        return $new;
    }

    public static function secondToDate($timestamp, $timezone = '')
    {
        if ($timezone) {
            date_default_timezone_set($timezone);
        }
        return date('Y-m-d', $timestamp);
    }

    public static function secondToDateTime($timestamp, $timezone = '')
    {
        if ($timezone) {
            date_default_timezone_set($timezone);
        }
        return date('Y-m-d H:i:s', $timestamp);
    }

    public static function toUnicode($unicodeStr)
    {
        foreach (self::$unicodeConvertChars as $from => $to) {
            $unicodeStr = str_replace($from, $to, $unicodeStr);
        }
        return $unicodeStr;
    }

    private static $unicodeConvertChars = ["é" => "é", "è" => "è", "ẹ" => "ẹ", "ẽ" => "ẽ", "ể" => "ể", "ế" => "ế", "ề" => "ề", "ệ" => "ệ", "ễ" => "ễ", "ỷ" => "ỷ", "ý" => "ý", "ỳ" => "ỳ", "ỵ" => "ỵ", "ỹ" => "ỹ", "ủ" => "ủ", "ú" => "ú", "ù" => "ù", "ụ" => "ụ", "ũ" => "ũ", "ử" => "ử", "ứ" => "ứ", "ừ" => "ừ", "ự" => "ự", "ữ" => "ữ", "ỉ" => "ỉ", "í" => "í", "ì" => "ì", "ị" => "ị", "ĩ" => "ĩ", "ỏ" => "ỏ", "ó" => "ó", "ò" => "ò", "ọ" => "ọ", "õ" => "õ", "ở" => "ở", "ớ" => "ớ", "ờ" => "ờ", "ợ" => "ợ", "ỡ" => "ỡ", "ổ" => "ổ", "ố" => "ố", "ồ" => "ồ", "ộ" => "ộ", "ỗ" => "ỗ", "ả" => "ả", "á" => "á", "à" => "à", "ạ" => "ạ", "ã" => "ã", "ẳ" => "ẳ", "ắ" => "ắ", "ằ" => "ằ", "ặ" => "ặ", "ẵ" => "ẵ", "ẩ" => "ẩ", "ấ" => "ấ", "ầ" => "ầ", "ậ" => "ậ", "ẫ" => "ẫ", "Ẻ" => "Ẻ", "É" => "É", "È" => "È", "Ẹ" => "Ẹ", "Ẽ" => "Ẽ", "Ể" => "Ể", "Ế" => "Ế", "Ề" => "Ề", "Ệ" => "Ệ", "Ễ" => "Ễ", "Ỷ" => "Ỷ", "Ý" => "Ý", "Ỳ" => "Ỳ", "Ỵ" => "Ỵ", "Ỹ" => "Ỹ", "Ủ" => "Ủ", "Ú" => "Ú", "Ù" => "Ù", "Ụ" => "Ụ", "Ũ" => "Ũ", "Ử" => "Ử", "Ứ" => "Ứ", "Ừ" => "Ừ", "Ự" => "Ự", "Ữ" => "Ữ", "Ỉ" => "Ỉ", "Í" => "Í", "Ì" => "Ì", "Ị" => "Ị", "Ĩ" => "Ĩ", "Ỏ" => "Ỏ", "Ó" => "Ó", "Ò" => "Ò", "Ọ" => "Ọ", "Õ" => "Õ", "Ở" => "Ở", "Ớ" => "Ớ", "Ờ" => "Ờ", "Ợ" => "Ợ", "Ỡ" => "Ỡ", "Ổ" => "Ổ", "Ố" => "Ố", "Ồ" => "Ồ", "Ộ" => "Ộ", "Ỗ" => "Ỗ", "Ả" => "Ả", "Á" => "Á", "À" => "À", "Ạ" => "Ạ", "Ã" => "Ã", "Ẳ" => "Ẳ", "Ắ" => "Ắ", "Ằ" => "Ằ", "Ặ" => "Ặ", "Ẵ" => "Ẵ", "Ẩ" => "Ẩ", "Ấ" => "Ấ", "Ầ" => "Ầ", "Ậ" => "Ậ", "Ẫ" => "Ẫ",];
}