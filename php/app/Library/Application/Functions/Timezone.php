<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 15/09/2019
 * Time: 11:37
 */

namespace App\Library\Application\Functions;



class Timezone
{
    static private $TIMEZONE_DATA = [
        1 => 'Asia/Ho_Chi_Minh',
        2 => 'Asia/Kuala_Lumpur',
        3 => 'Asia/Singapore',
        4 => 'Asia/Manila',
        5 => 'Asia/Jakarta',
        6 => 'Asia/Bangkok',
    ];

    /**
     * set By Country Id
     * @param int $id
     */
    public static function setByVentureId($id)
    {
        date_default_timezone_set(self::$TIMEZONE_DATA[$id] ?? self::$TIMEZONE_DATA[1]);
    }

    const HO_CHI_MINH_TIMEZONE = 1;
    const KUALA_LUMPUR_TIMEZONE = 2;
    const SINGAPORE_TIMEZONE = 3;
    const MANILA_TIMEZONE = 4;
    const JAKARTA_TIMEZONE = 5;
    const BANGKOK_TIMEZONE = 6;
}