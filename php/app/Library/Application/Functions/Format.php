<?php
/**
 * Created by PhpStorm.
 * User: phatle
 * Date: 10/08/2018
 * Time: 08:50
 */

namespace App\Library\Application\Functions;

class Format
{
    /**
     * @param mixed $date
     * @return mixed
     */
    public static function date($date)
    {
        return date('Y-m-d', strtotime($date));
    }

    /**
     * @param mixed $datetime
     * @return mixed
     */
    public static function dateTime($datetime)
    {
        return date('Y-m-d H:i:s', strtotime($datetime));
    }
}