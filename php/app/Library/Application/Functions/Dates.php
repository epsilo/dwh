<?php
/**
 * Created by PhpStorm.
 * User: phatle
 * Date: 10/08/2018
 * Time: 08:50
 */

namespace App\Library\Application\Functions;

class Dates
{
    /**
     * @param mixed $date1
     * @param mixed $date2
     * @return mixed
     */
    public static function dateDiff($date1, $date2)
    {
        $date1 = new \DateTime($date1);
        $date2 = new \DateTime($date2);
        return $date1->diff($date2)->days;
    }
}