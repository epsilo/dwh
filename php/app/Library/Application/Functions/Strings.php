<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 15/09/2019
 * Time: 22:18
 */

namespace App\Library\Application\Functions;


class Strings
{

    public static function randomString($length = 8)
    {
        $characters = '123456789ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}