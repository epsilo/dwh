<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:59
 */
namespace App\Library\Application\Factories\Channels;

interface IChannel
{
    /**
     * @param $fullJson
     * @return float
     */
    public function getRrpFromItemFullJson($fullJson);

    /**
     * @param $fullJson
     * @param $addressCity
     * @return array
     */
    public function getGeoFromAddress($fullJson, $addressCity);

    /**
     * @param string $orderStatus
     * @param string $itemStatus
     * @param string $orderFullJson
     * @param string $itemFullJson
     * @return mixed
     */
    public function convertStatus($orderStatus, $itemStatus, $orderFullJson, $itemFullJson);

    const UNKNOWN_STATUS = 'Unknown';
    const PENDING = 'Pending';
    const DELIVERED = 'Delivered';
    const CANCELED = 'Canceled';
    const READY_TO_SHIP = 'Ready_to_ship';
    const FAILED = 'Failed';
    const RETURNED = 'Returned';
    const SHIPPED = 'Shipped';
    const HANDOVER_TO_3PL = 'Handover_to_3pl';
}