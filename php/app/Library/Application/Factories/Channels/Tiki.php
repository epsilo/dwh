<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:59
 */

namespace App\Library\Application\Factories\Channels;

use App\Library\Application\Factories\Channels;

class Tiki implements Channels\IChannel
{

    public function getRrpFromItemFullJson($fullJson)
    {
        return 0;
    }

    /**
     * @param $fullJson
     * @param $addressCity
     * @return array
     */
    public function getGeoFromAddress($fullJson, $addressCity)
    {
        $saleOrderData = json_decode($fullJson, true);
        $street = $saleOrderData['shipping_address']['street'] ?? '';
        $ward = $saleOrderData['shipping_address']['ward'] ?? '';
        $city = $saleOrderData['shipping_address']['city'] ?? '';
        $region = $saleOrderData['shipping_address']['region'] ?? '';
        $country = $saleOrderData['shipping_address']['country'] ?? '';
        $stateProvince = $region;
        $address = trim($street . ' ' . $ward. ' ' . $city . ' ' . $region . ' ' . $country);
        $address = $address ? $address :	$addressCity;
        return [
            "street" => $street,
            "route" => null,
            "district" => $ward,
            'shipping_address' => $address,
            'state_province' => self::$unicodeStateData[$stateProvince] ?? $stateProvince,
            'region' => null,
            'country' => $country,
            'shipping_city' => $city,
        ];

    }

    /**
     * @param string $orderStatus
     * @param string $itemStatus
     * @param string $orderFullJson
     * @param string $itemFullJson
     * @return mixed
     */
    public function convertStatus($orderStatus, $itemStatus, $orderFullJson, $itemFullJson)
    {
        $result = self::$statusMappingData[$orderStatus] ?? IChannel::UNKNOWN_STATUS;
        return $result;
    }

    private static $statusMappingData = [
        'processing' => IChannel::PENDING,
        'doi_thanh_toan' => IChannel::PENDING,
        'da_thanh_toan' => IChannel::PENDING,
        'cho_in' => IChannel::PENDING,
        'phan_cong_lay_hang' => IChannel::HANDOVER_TO_3PL,
        'dang_lay_hang' => IChannel::HANDOVER_TO_3PL,
        'dang_dong_goi' => IChannel::HANDOVER_TO_3PL,
        'dong_goi_xong' => IChannel::HANDOVER_TO_3PL,
        'len_ke' => IChannel::HANDOVER_TO_3PL,
        'ban_giao_doi_tac' => IChannel::SHIPPED,
        'dang_van_chuyen' => IChannel::SHIPPED,
        'giao_hang_thanh_cong' => IChannel::DELIVERED,
        'complete' => IChannel::DELIVERED,
        'canceled' => IChannel::CANCELED,
        'holded' => IChannel::UNKNOWN_STATUS,
        'returned' => IChannel::RETURNED,
    ];

    private static $unicodeStateData = [
        'กรุงเทพมหานคร' => 'Bangkok',
        'อำนาจเจริญ' => 'Amnat Charoen',
        'อ่างทอง' => 'Ang Thong',
        'บึงกาฬ' => 'Bueng Kan',
        'บุรีรัมย์' => 'Buriram',
        'ฉะเชิงเทรา' => 'Chachoengsao',
        'ชัยนาท' => 'Chai Nat',
        'ชัยภูมิ' => 'Chaiyaphum',
        'จันทบุรี' => 'Chanthaburi',
        'เชียงใหม่' => 'Chiang Mai',
        'เชียงราย' => 'Chiang Rai',
        'ชลบุรี' => 'Chonburi',
        'ชุมพร' => 'Chumphon',
        'กาฬสินธุ์' => 'Kalasin',
        'กำแพงเพชร' => 'Kamphaeng Phet',
        'กาญจนบุรี' => 'Kanchanaburi',
        'ขอนแก่น' => 'Khon Kaen',
        'กระบี่' => 'Krabi',
        'ลำปาง' => 'Lampang',
        'ลำพูน' => 'Lamphun',
        'เลย' => 'Loei',
        'ลพบุรี' => 'Lopburi',
        'แม่ฮ่องสอน' => 'Mae Hong Son',
        'มหาสารคาม' => 'Maha Sarakham',
        'มุกดาหาร' => 'Mukdahan',
        'นครนายก' => 'Nakhon Nayok',
        'นครปฐม' => 'Nakhon Pathom',
        'นครพนม' => 'Nakhon Phanom',
        'นครราชสีมา' => 'Nakhon Ratchasima',
        'นครสวรรค์' => 'Nakhon Sawan',
        'นครศรีธรรมราช' => 'Nakhon Si Thammarat',
        'น่าน' => 'Nan',
        'นราธิวาส' => 'Narathiwat',
        'หนองบัวลำภู' => 'Nong Bua Lamphu',
        'หนองคาย' => 'Nong Khai',
        'นนทบุรี' => 'Nonthaburi',
        'ปทุมธานี' => 'Pathum Thani',
        'ปัตตานี' => 'Pattani',
        'พังงา' => 'Phang Nga',
        'พัทลุง' => 'Phatthalung',
        'พะเยา' => 'Phayao',
        'เพชรบูรณ์' => 'Phetchabun',
        'เพชรบุรี' => 'Phetchaburi',
        'พิจิตร' => 'Phichit',
        'พิษณุโลก' => 'Phitsanulok',
        'พระนครศรีอยุธยา' => 'Phra Nakhon Si Ayutthaya',
        'แพร่' => 'Phrae',
        'ภูเก็ต' => 'Phuket',
        'ปราจีนบุรี' => 'Prachinburi',
        'ประจวบคีรีขันธ์' => 'Prachuap Khiri Khan',
        'ระนอง' => 'Ranong',
        'ราชบุรี' => 'Ratchaburi',
        'ระยอง' => 'Rayong',
        'ร้อยเอ็ด' => 'Roi Et',
        'สระแก้ว' => 'Sa Kaeo',
        'สกลนคร' => 'Sakon Nakhon',
        'สมุทรปราการ' => 'Samut Prakan',
        'สมุทรสาคร' => 'Samut Sakhon',
        'สมุทรสงคราม' => 'Samut Songkhram',
        'สระบุรี' => 'Saraburi',
        'สตูล' => 'Satun',
        'สิงห์บุรี' => 'Sing Buri',
        'ศรีสะเกษ' => 'Sisaket',
        'สงขลา' => 'Songkhla',
        'สุโขทัย' => 'Sukhothai',
        'สุพรรณบุรี' => 'Suphan Buri',
        'สุราษฎร์ธานี' => 'Surat Thani',
        'สุรินทร์' => 'Surin',
        'ตาก' => 'Tak',
        'ตรัง' => 'Trang',
        'ตราด' => 'Trat',
        'อุบลราชธานี' => 'Ubon Ratchathani',
        'อุดรธานี' => 'Udon Thani',
        'อุทัยธานี' => 'Uthai Thani',
        'อุตรดิตถ์' => 'Uttaradit',
        'ยะลา' => 'Yala',
        'ยโสธร ' => 'Yasothon',
    ];
}