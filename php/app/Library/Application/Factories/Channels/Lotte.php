<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:59
 */

namespace App\Library\Application\Factories\Channels;

use App\Library\Application\Factories\Channels;

class Lotte implements Channels\IChannel
{

    public function getRrpFromItemFullJson($fullJson)
    {
        return 0;
    }

    /**
     * @param $fullJson
     * @param $addressCity
     * @return array
     */
    public function getGeoFromAddress($fullJson, $addressCity)
    {
        $data = json_decode($fullJson, true);
        $stateProvince = $data['shippingAddress']['city'] ?? '';
        $city = $data['shippingAddress']['ward'] ?? '';
        $district = $data['shippingAddress']['district'] ?? '';
        $address = $data['shippingAddress']['address1'] ?? $addressCity;
        return [
            "street" => null,
            "route" => null,
            "district" => $district,
            'shipping_address' => $address,
            'state_province' => $stateProvince,
            'region' => null,
            'country' => null,
            'shipping_city' => $city,
        ];
    }

    /**
     * @param string $orderStatus
     * @param string $itemStatus
     * @param string $orderFullJson
     * @param string $itemFullJson
     * @return mixed
     */
    public function convertStatus($orderStatus, $itemStatus, $orderFullJson, $itemFullJson)
    {
        $result = self::$statusMappingData[$itemStatus] ?? IChannel::UNKNOWN_STATUS;
        return $result;
    }

    private static $statusMappingData = [
        '20' => self::PENDING,
        '21' => self::PENDING,
        '23' => self::READY_TO_SHIP,
        '24' => self::DELIVERED,
        '25' => self::SHIPPED,
        '26' => self::CANCELED,
        '30' => self::UNKNOWN_STATUS,
        '31' => self::UNKNOWN_STATUS,
        '32' => self::FAILED,
        '36' => self::UNKNOWN_STATUS,
    ];

    private static $unicodeStateData = [
    ];
}