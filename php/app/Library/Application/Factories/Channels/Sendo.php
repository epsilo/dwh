<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:59
 */

namespace App\Library\Application\Factories\Channels;

use App\Library\Application\Factories\Channels;

class Sendo implements Channels\IChannel
{

    public function getRrpFromItemFullJson($fullJson)
    {
        return 0;
    }

    /**
     * @param $fullJson
     * @param $addressCity
     * @return array
     */
    public function getGeoFromAddress($fullJson, $addressCity)
    {
        $data = json_decode($fullJson, true);
        $address = $data['regionname'] ?? $addressCity;
        $addressData = array_map('trim', explode(',', $address));
        $stateProvince = count($addressData) >= 2 ? $addressData[count($addressData) - 1] : null;
        $city = count($addressData) >= 2 ? $addressData[count($addressData) - 2] : null;
        $stateProvince = str_replace('–', '-', $stateProvince);
        $city = str_replace('–', '-', $city);
        return [
            "street" => null,
            "route" => null,
            "district" => null,
            'shipping_address' => $address,
            'state_province' => $stateProvince,
            'region' => null,
            'country' => null,
            'shipping_city' => $city,
        ];
    }

    /**
     * @param string $orderStatus
     * @param string $itemStatus
     * @param string $orderFullJson
     * @param string $itemFullJson
     * @return mixed
     */
    public function convertStatus($orderStatus, $itemStatus, $orderFullJson, $itemFullJson)
    {
        $result = self::$statusMappingData[$orderStatus] ?? IChannel::UNKNOWN_STATUS;
        return $result;
    }

    private static $statusMappingData = [
        'New' => self::PENDING,
        'Proccessing' => self::PENDING,
        'Shipping' => self::SHIPPED,
        'POD' => self::DELIVERED,
        'Completed' => self::DELIVERED,
        'Closed' => self::DELIVERED,
        'Cancelled' => self::CANCELED,
    ];

    private static $unicodeStateData = [
    ];
}