<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:59
 */

namespace App\Library\Application\Factories\Channels;

use App\Library\Application\Factories\Channels;

class Shopee implements Channels\IChannel
{
    /**
     * @param $fullJson
     * @return float
     */
    public function getRrpFromItemFullJson($fullJson)
    {
        $data = json_decode($fullJson, true);
        return floatval($data['variation_original_price'] ?? 0);
    }

    /**
     * @param $fullJson
     * @param $addressCity
     * @return array
     */
    public function getGeoFromAddress($fullJson, $addressCity)
    {
        $saleOrderData = json_decode($fullJson, true);
        $country = $saleOrderData['recipient_address']['country'] ?? '';
        $stateProvince = $saleOrderData['recipient_address']['state'] ?? '';
        $city = $saleOrderData['recipient_address']['city'] ?? '';
        $district = $saleOrderData['recipient_address']['district'] ?? '';
        $fullAddress = $saleOrderData['recipient_address']['full_address'] ?? $addressCity;
        return [
            "street" => null,
            "route" => null,
            "district" => $district,
            'shipping_address' => $fullAddress,
            'state_province' => self::$unicodeStateData[$stateProvince] ?? $stateProvince,
            'region' => null,
            'country' => $country,
            'shipping_city' => $city,
        ];
    }

    /**
     * @param string $orderStatus
     * @param string $itemStatus
     * @param string $orderFullJson
     * @param string $itemFullJson
     * @return mixed
     */
    public function convertStatus($orderStatus, $itemStatus, $orderFullJson, $itemFullJson)
    {
        $result = self::$statusMappingData[$itemStatus] ?? IChannel::UNKNOWN_STATUS;
        if ($result == IChannel::PENDING) {
            $data = json_decode($orderFullJson, true);
            $result = $data['tracking_no'] ?? null ? IChannel::READY_TO_SHIP : IChannel::PENDING;
        }
        return $result;
    }

    private static $statusMappingData = [
        'READY_TO_SHIP' => IChannel::PENDING,
        'SHIPPED' => IChannel::SHIPPED,
        'CANCELLED' => IChannel::CANCELED,
        # 'TO_RETURN' => self::SHIPPED,
        # 'IN_CANCEL' => self::PENDING,
        'TO_CONFIRM_RECEIVE' => IChannel::SHIPPED,
        'COMPLETED' => IChannel::DELIVERED,
        'INVALID' => IChannel::UNKNOWN_STATUS,
        'RETRY_SHIP' => IChannel::UNKNOWN_STATUS,
        'UNPAID' => IChannel::PENDING,
    ];

    private static $unicodeStateData = [
        'กรุงเทพมหานคร' => 'Bangkok',
        'อำนาจเจริญ' => 'Amnat Charoen',
        'อ่างทอง' => 'Ang Thong',
        'บึงกาฬ' => 'Bueng Kan',
        'บุรีรัมย์' => 'Buriram',
        'ฉะเชิงเทรา' => 'Chachoengsao',
        'ชัยนาท' => 'Chai Nat',
        'ชัยภูมิ' => 'Chaiyaphum',
        'จันทบุรี' => 'Chanthaburi',
        'เชียงใหม่' => 'Chiang Mai',
        'เชียงราย' => 'Chiang Rai',
        'ชลบุรี' => 'Chonburi',
        'ชุมพร' => 'Chumphon',
        'กาฬสินธุ์' => 'Kalasin',
        'กำแพงเพชร' => 'Kamphaeng Phet',
        'กาญจนบุรี' => 'Kanchanaburi',
        'ขอนแก่น' => 'Khon Kaen',
        'กระบี่' => 'Krabi',
        'ลำปาง' => 'Lampang',
        'ลำพูน' => 'Lamphun',
        'เลย' => 'Loei',
        'ลพบุรี' => 'Lopburi',
        'แม่ฮ่องสอน' => 'Mae Hong Son',
        'มหาสารคาม' => 'Maha Sarakham',
        'มุกดาหาร' => 'Mukdahan',
        'นครนายก' => 'Nakhon Nayok',
        'นครปฐม' => 'Nakhon Pathom',
        'นครพนม' => 'Nakhon Phanom',
        'นครราชสีมา' => 'Nakhon Ratchasima',
        'นครสวรรค์' => 'Nakhon Sawan',
        'นครศรีธรรมราช' => 'Nakhon Si Thammarat',
        'น่าน' => 'Nan',
        'นราธิวาส' => 'Narathiwat',
        'หนองบัวลำภู' => 'Nong Bua Lamphu',
        'หนองคาย' => 'Nong Khai',
        'นนทบุรี' => 'Nonthaburi',
        'ปทุมธานี' => 'Pathum Thani',
        'ปัตตานี' => 'Pattani',
        'พังงา' => 'Phang Nga',
        'พัทลุง' => 'Phatthalung',
        'พะเยา' => 'Phayao',
        'เพชรบูรณ์' => 'Phetchabun',
        'เพชรบุรี' => 'Phetchaburi',
        'พิจิตร' => 'Phichit',
        'พิษณุโลก' => 'Phitsanulok',
        'พระนครศรีอยุธยา' => 'Phra Nakhon Si Ayutthaya',
        'แพร่' => 'Phrae',
        'ภูเก็ต' => 'Phuket',
        'ปราจีนบุรี' => 'Prachinburi',
        'ประจวบคีรีขันธ์' => 'Prachuap Khiri Khan',
        'ระนอง' => 'Ranong',
        'ราชบุรี' => 'Ratchaburi',
        'ระยอง' => 'Rayong',
        'ร้อยเอ็ด' => 'Roi Et',
        'สระแก้ว' => 'Sa Kaeo',
        'สกลนคร' => 'Sakon Nakhon',
        'สมุทรปราการ' => 'Samut Prakan',
        'สมุทรสาคร' => 'Samut Sakhon',
        'สมุทรสงคราม' => 'Samut Songkhram',
        'สระบุรี' => 'Saraburi',
        'สตูล' => 'Satun',
        'สิงห์บุรี' => 'Sing Buri',
        'ศรีสะเกษ' => 'Sisaket',
        'สงขลา' => 'Songkhla',
        'สุโขทัย' => 'Sukhothai',
        'สุพรรณบุรี' => 'Suphan Buri',
        'สุราษฎร์ธานี' => 'Surat Thani',
        'สุรินทร์' => 'Surin',
        'ตาก' => 'Tak',
        'ตรัง' => 'Trang',
        'ตราด' => 'Trat',
        'อุบลราชธานี' => 'Ubon Ratchathani',
        'อุดรธานี' => 'Udon Thani',
        'อุทัยธานี' => 'Uthai Thani',
        'อุตรดิตถ์' => 'Uttaradit',
        'ยะลา' => 'Yala',
        'ยโสธร ' => 'Yasothon',
    ];
}