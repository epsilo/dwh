<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:59
 */

namespace App\Library\Application\Factories\Channels;

use App\Library\Application\Factories\Channels;

class Lazada implements Channels\IChannel
{

    public function getRrpFromItemFullJson($fullJson)
    {
        return 0;
    }

    /**
     * @param $fullJson
     * @param $addressCity
     * @return array
     */
    public function getGeoFromAddress($fullJson, $addressCity)
    {
        $saleOrderData = json_decode($fullJson, true);
        $address1 = $saleOrderData['address_billing']['address1'] ?? $addressCity;
        $address2 = $saleOrderData['address_billing']['address2'] ?? '';
        $address3 = $saleOrderData['address_billing']['address3'] ?? '';
        $address4 = $saleOrderData['address_billing']['address4'] ?? '';
        $address5 = $saleOrderData['address_billing']['address5'] ?? '';
        $country = $saleOrderData['address_billing']['country'] ?? '';
        $city = $saleOrderData['address_billing']['city'] ?? '';
        $stateData = explode('/ ', $address3);
        $stateProvince = count($stateData) == 2 ? trim($stateData[1]) : $address3;
        $cityData = explode('/ ', $city);
        $city = count($cityData) == 2 ? trim($cityData[1]) : $city;
        return [
            "street" => $address1,
            "route" => null,
            "district" => $address4,
            'shipping_address' => $addressCity,
            'state_province' => self::$unicodeStateData[$stateProvince] ?? $stateProvince,
            'region' => null,
            'country' => $country,
            'shipping_city' => $city,
        ];
    }

    /**
     * @param string $orderStatus
     * @param string $itemStatus
     * @param string $orderFullJson
     * @param string $itemFullJson
     * @return mixed
     */
    public function convertStatus($orderStatus, $itemStatus, $orderFullJson, $itemFullJson)
    {
        $result = self::$statusMappingData[$itemStatus] ?? IChannel::UNKNOWN_STATUS;
        return $result;
    }

    private static $statusMappingData = [
        'pending' => IChannel::PENDING,
        'delivered' => IChannel::DELIVERED,
        'canceled' => IChannel::CANCELED,
        'ready_to_ship' => IChannel::READY_TO_SHIP,
        'delivered_fail' => IChannel::FAILED,
        'failed' => IChannel::FAILED,
        'returned' => IChannel::RETURNED,
        'shipped' => IChannel::SHIPPED,
    ];

    private static $unicodeStateData = [
        'กรุงเทพมหานคร' => 'Bangkok',
        'อำนาจเจริญ' => 'Amnat Charoen',
        'อ่างทอง' => 'Ang Thong',
        'บึงกาฬ' => 'Bueng Kan',
        'บุรีรัมย์' => 'Buriram',
        'ฉะเชิงเทรา' => 'Chachoengsao',
        'ชัยนาท' => 'Chai Nat',
        'ชัยภูมิ' => 'Chaiyaphum',
        'จันทบุรี' => 'Chanthaburi',
        'เชียงใหม่' => 'Chiang Mai',
        'เชียงราย' => 'Chiang Rai',
        'ชลบุรี' => 'Chonburi',
        'ชุมพร' => 'Chumphon',
        'กาฬสินธุ์' => 'Kalasin',
        'กำแพงเพชร' => 'Kamphaeng Phet',
        'กาญจนบุรี' => 'Kanchanaburi',
        'ขอนแก่น' => 'Khon Kaen',
        'กระบี่' => 'Krabi',
        'ลำปาง' => 'Lampang',
        'ลำพูน' => 'Lamphun',
        'เลย' => 'Loei',
        'ลพบุรี' => 'Lopburi',
        'แม่ฮ่องสอน' => 'Mae Hong Son',
        'มหาสารคาม' => 'Maha Sarakham',
        'มุกดาหาร' => 'Mukdahan',
        'นครนายก' => 'Nakhon Nayok',
        'นครปฐม' => 'Nakhon Pathom',
        'นครพนม' => 'Nakhon Phanom',
        'นครราชสีมา' => 'Nakhon Ratchasima',
        'นครสวรรค์' => 'Nakhon Sawan',
        'นครศรีธรรมราช' => 'Nakhon Si Thammarat',
        'น่าน' => 'Nan',
        'นราธิวาส' => 'Narathiwat',
        'หนองบัวลำภู' => 'Nong Bua Lamphu',
        'หนองคาย' => 'Nong Khai',
        'นนทบุรี' => 'Nonthaburi',
        'ปทุมธานี' => 'Pathum Thani',
        'ปัตตานี' => 'Pattani',
        'พังงา' => 'Phang Nga',
        'พัทลุง' => 'Phatthalung',
        'พะเยา' => 'Phayao',
        'เพชรบูรณ์' => 'Phetchabun',
        'เพชรบุรี' => 'Phetchaburi',
        'พิจิตร' => 'Phichit',
        'พิษณุโลก' => 'Phitsanulok',
        'พระนครศรีอยุธยา' => 'Phra Nakhon Si Ayutthaya',
        'แพร่' => 'Phrae',
        'ภูเก็ต' => 'Phuket',
        'ปราจีนบุรี' => 'Prachinburi',
        'ประจวบคีรีขันธ์' => 'Prachuap Khiri Khan',
        'ระนอง' => 'Ranong',
        'ราชบุรี' => 'Ratchaburi',
        'ระยอง' => 'Rayong',
        'ร้อยเอ็ด' => 'Roi Et',
        'สระแก้ว' => 'Sa Kaeo',
        'สกลนคร' => 'Sakon Nakhon',
        'สมุทรปราการ' => 'Samut Prakan',
        'สมุทรสาคร' => 'Samut Sakhon',
        'สมุทรสงคราม' => 'Samut Songkhram',
        'สระบุรี' => 'Saraburi',
        'สตูล' => 'Satun',
        'สิงห์บุรี' => 'Sing Buri',
        'ศรีสะเกษ' => 'Sisaket',
        'สงขลา' => 'Songkhla',
        'สุโขทัย' => 'Sukhothai',
        'สุพรรณบุรี' => 'Suphan Buri',
        'สุราษฎร์ธานี' => 'Surat Thani',
        'สุรินทร์' => 'Surin',
        'ตาก' => 'Tak',
        'ตรัง' => 'Trang',
        'ตราด' => 'Trat',
        'อุบลราชธานี' => 'Ubon Ratchathani',
        'อุดรธานี' => 'Udon Thani',
        'อุทัยธานี' => 'Uthai Thani',
        'อุตรดิตถ์' => 'Uttaradit',
        'ยะลา' => 'Yala',
        'ยโสธร ' => 'Yasothon',
    ];
}