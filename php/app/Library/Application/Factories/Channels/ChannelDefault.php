<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:59
 */

namespace App\Library\Application\Factories\Channels;
class ChannelDefault implements IChannel
{
    /**
     * @param $fullJson
     * @return float
     */
    public function getRrpFromItemFullJson($fullJson)
    {
        return 0;
    }

    /**
     * @param $fullJson
     * @param $addressCity
     * @return array
     */
    public function getGeoFromAddress($fullJson, $addressCity)
    {
        $street = null;
        $route = null;
        $country = null;
        $city = null;
        $stateProvince = null;
        $district = null;
        /*/
        $google = new \App\Library\Application\Google\Api\Geocode();
        $data = $google->getGeoLocation($addressCity);
        if ($data["status"] == "OK") {
            $componentData = $data['results'][0]['address_components'];
            foreach ($componentData as $component) {
                $type = $component['types'][0] ?? null;
                switch ($type) {
                    case "street_number":
                        $street = $component['long_name'];
                        break;
                    case "route":
                        $route = $component['long_name'];
                        break;
                    case "locality":
                        $city = $component['long_name'];
                        break;
                    case "administrative_area_level_1":
                        $stateProvince = $component['long_name'];
                        break;
                    case "administrative_area_level_2":
                        $district = $component['long_name'];
                        break;
                    case "country":
                        $country = $component['long_name'];
                        break;
                }
            }
        }//*/
        return [
            "street" => $street,
            "route" => $route,
            "district" => $district,
            'shipping_address' => $addressCity,
            'state_province' => $stateProvince,
            'region' => null,
            'country' => $country,
            'shipping_city' => $city,
        ];
    }

    /**
     * @param string $orderStatus
     * @param string $itemStatus
     * @param string $orderFullJson
     * @param string $itemFullJson
     * @return mixed
     */
    public function convertStatus($orderStatus, $itemStatus, $orderFullJson, $itemFullJson)
    {
        return $itemStatus;
    }
}