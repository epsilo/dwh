<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:59
 */

namespace App\Library\Application\Factories\Channels;

use App\Library\Application\Factories\Channels;

class Adayroi implements Channels\IChannel
{

    public function getRrpFromItemFullJson($fullJson)
    {
        $data = json_decode($fullJson, true);
        return floatval($data['OriginalPrice'] ?? 0);
    }

    /**
     * @param $fullJson
     * @param $addressCity
     * @return array
     */
    public function getGeoFromAddress($fullJson, $addressCity)
    {
        return [
            "street" => null,
            "route" => null,
            "district" => null,
            'shipping_address' => $addressCity,
            'state_province' => null,
            'region' => null,
            'country' => null,
            'shipping_city' => null,
        ];
    }

    /**
     * @param string $orderStatus
     * @param string $itemStatus
     * @param string $orderFullJson
     * @param string $itemFullJson
     * @return mixed
     */
    public function convertStatus($orderStatus, $itemStatus, $orderFullJson, $itemFullJson)
    {
        $result = self::$statusMappingData[$itemStatus] ?? IChannel::UNKNOWN_STATUS;
        return $result;
    }

    private static $statusMappingData = [
        'WAITING_FOR_MC_TO_CONFIRM' => self::PENDING,
        'MC_CONFIRMED_WAITING_FOR_CS_TO_CONFIRM' => self::PENDING,
        'CS_CONFIRMED_WAITING_FOR_LOGISTICS_TO_PROCESS' => self::READY_TO_SHIP,
        'PICKING' => self::READY_TO_SHIP,
        'PICKED' => self::HANDOVER_TO_3PL,
        'WAITING_FOR_DELIVERY' => self::SHIPPED,
        'DELIVERING' => self::SHIPPED,
        'DELIVERED' => self::DELIVERED,
        'CLOSED' => self::DELIVERED,
        'REQUEST_FOR_RETURNING' => self::DELIVERED,
        'CANCELLED' => self::CANCELED,
        'REFUSED_FOR_RETURNING' => self::DELIVERED,
        'RETURNED' => self::RETURNED,
        'FAILED_PICKING' => self::FAILED,
        'FAILED_DELIVERY' => self::FAILED,
        'IMPORTED_RETURNED_PRODUCT' => self::RETURNED,
        'RETURNING_TO_MC' => self::RETURNED,
        'FAILED_RETURN_TO_MC' => self::RETURNED,
        'RETURNED_TO_MC' => self::RETURNED,
        'IMPORTED_WAITING_FOR_MOVING_TO_WRITE_OFF_WAREHOUSE' => self::UNKNOWN_STATUS,
        'IMPORTED_WRITE_OFF_WAREHOUSE_TO_BE_EXAMINED' => self::UNKNOWN_STATUS,
        'EXAMINED' => self::UNKNOWN_STATUS,
    ];

    private static $unicodeStateData = [
    ];
}