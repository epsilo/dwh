<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/10/2019
 * Time: 09:58
 */

namespace App\Library\Application\Factories;
use App\Library\Application\Factories\Channels;


class Channel
{
    private static $factoryObject = [];

    /**
     * @param $channelCode
     * @return \App\Library\Application\Factories\Channels\IChannel
     */
    public static function get($channelCode)
    {
        $channelCode = trim(strtoupper($channelCode));
        if (!(self::$factoryObject[$channelCode] ?? null)) {
            switch ($channelCode) {
                case  self::TIKI_CODE: self::$factoryObject[$channelCode] = new Channels\Tiki(); break;
                case  self::LAZADA_CODE: self::$factoryObject[$channelCode] = new Channels\Lazada(); break;
                case  self::SHOPEE_CODE: self::$factoryObject[$channelCode] = new Channels\Shopee(); break;
                case  self::ADAYROI_CODE: self::$factoryObject[$channelCode] = new Channels\Adayroi(); break;
                case  self::LOTTE_CODE: self::$factoryObject[$channelCode] = new Channels\Lotte(); break;
                case  self::SENDO_CODE: self::$factoryObject[$channelCode] = new Channels\Sendo(); break;
                default: $channelCode = 'default'; self::$factoryObject[$channelCode] = new Channels\ChannelDefault(); break;
            }

        }
        return self::$factoryObject[$channelCode];
    }

    const ADAYROI_CODE = 'ADAYROI';
    const LOTTE_CODE = 'LOTTE';
    const SENDO_CODE = 'SENDO';
    const TIKI_CODE = 'TIKI';
    const LAZADA_CODE = 'LAZADA';
    const SHOPEE_CODE = 'SHOPEE';
}