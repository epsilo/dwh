<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 08/11/2019
 * Time: 15:00
 */

namespace App\Library\Application;


class DataEncryption
{
    private function __construct() {}


    /**
     * @param $encryptData
     * @return string
     */
    public static function dataDecrypt($encryptData)
    {
        $fourth = substr($encryptData,4,4);
        $encryptData = substr_replace($encryptData,'', 4,4);
        $third = substr($encryptData,0,4);
        $encryptData = substr_replace($encryptData,'', 0,4);
        $second = substr($encryptData,5,4);
        $encryptData = substr_replace($encryptData,'', 5,4);
        $first = substr($encryptData,1,4);
        $encryptData = substr_replace($encryptData,'', 1,4);
        $key = self::getEncryptKey();
        $iv = $first.$second.$third.$fourth;
        return openssl_decrypt(hex2bin($encryptData), self::CIPHER, $key, $options = 0, $iv);
    }

    /**
     * @param string|array|object $dataString
     * @return array ['data' => <DATA_STRING>, 'iv' => <GENERATE_IV>]
     */
    public static function dataEncrypt($dataString)
    {
        if (!is_string($dataString)) {
            $dataString = json_encode($dataString);
        }
        $key = self::getEncryptKey();
        $iv = self::generateIv();
        $encryptString = strtoupper(bin2hex(openssl_encrypt($dataString, self::CIPHER, $key, $options = 0, $iv)));
        $first = substr($iv, 0, 4);
        $second = substr($iv, 4, 4);
        $third = substr($iv, 8, 4);
        $fourth = substr($iv, 12, 4);
        $encryptString = substr_replace($encryptString, $first, 1, 0);
        $encryptString = substr_replace($encryptString, $second, 5, 0);
        $encryptString = substr_replace($encryptString, $third, 0, 0);
        $encryptString = substr_replace($encryptString, $fourth, 4, 0);
        return $encryptString;
    }

    private static function getEncryptKey()
    {
        return file_get_contents(SYS_PATH.'/../credential/e_key');
    }

    private static function generateIv()
    {
        $characters = '0123456789ABCDEFGHJKMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 16; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    const CIPHER = 'aes-256-cbc-hmac-sha256';
}