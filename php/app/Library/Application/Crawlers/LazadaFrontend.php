<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 06/11/2019
 * Time: 16:01
 */

namespace App\Library\Application\Crawlers;


use PharIo\Version\Exception;

class LazadaFrontend
{

    # Product Info Group ----------------------------------------------------------------------------------------------#

    static public function getProductPageInfo($url, $proxy = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers[] = "User-Agent: " . self::$userAgentList[rand(0, count(self::$userAgentList) - 1)];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if ($proxy) {
            curl_setopt($ch, CURLOPT_PROXY, $proxy);
            print_r($proxy.PHP_EOL);
        }
        $result = curl_exec($ch);
        if(curl_getinfo($ch, CURLINFO_HTTP_CODE) != 200) return null;
        if (strpos($result, 'app.run({') === false) {
            if (strpos($result, 'x5secdata') === false) {
                return false;
            } else {
                return null;
            }
        }
        $string = substr($result, strpos($result, 'app.run({') + 8);
        $string = substr($string, 0, strpos($string, '});') + 1);
        return json_decode($string, true);
    }

    static public function formatStorageUrl($url)
    {
        if (!trim($url)) return null;
        $url = current(explode('.html', $url));
        $data = explode('/', $url.'.html');
        $linkData = explode('-', end($data));
        $endLink = '-' . implode('-', array_slice($linkData, count($linkData) - 2));
        array_pop($data);
        $data[] = $endLink;
        return trim(implode('/', $data));
    }

    static public function extractProductInfoProductName(&$productInfo)
    {
        return $productInfo['data']['root']['fields']['skuInfos']['0']['dataLayer']['pdt_name'] ?? null;
    }

    static public function extractProductInfoPrice(&$productInfo)
    {
        return $productInfo['data']['root']['fields']['skuInfos']['0']['price']['salePrice']['value'] ?? null;
    }

    #------------------------------------------------------------------------------------------------------------------#

    private static $userAgentList = [
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246',
        'Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36',
        'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1',
    ];
}