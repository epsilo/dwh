<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 06/11/2019
 * Time: 16:01
 */

namespace App\Library\Application\Crawlers;


use App\Library\Application\Constants;

class LazadaBackend
{
    private $countryId;
    private $cookiesPath;
    private $username;
    private $password;

    public function __construct($username, $password, $countryId = null)
    {
        $this->username = $username;
        $this->password = $password;
        $this->countryId = $countryId;
    }

    public function setCountryId($countryId)
    {
        $this->countryId = intval($countryId);
    }

    public function login()
    {
        $username = $this->username;
        $password = $this->password;
        if (!$username || !$password) return false;
        $ch = curl_init();
        $lazadaScUrl = $this->lazadaSellerCenterUrl();
        $lazadaScDomain = str_replace('https://', '', $lazadaScUrl);
        $cookiesPath = SYS_PATH . "/storage/crawl-cookies/lazada/$this->countryId/$username.txt";
        if (!file_exists(SYS_PATH . "/storage/crawl-cookies/")) mkdir(SYS_PATH . "/storage/crawl-cookies/");
        if (!file_exists(SYS_PATH . "/storage/crawl-cookies/lazada/")) mkdir(SYS_PATH . "/storage/crawl-cookies/lazada/");
        if (!file_exists(SYS_PATH . "/storage/crawl-cookies/lazada/$this->countryId/")) mkdir(SYS_PATH . "/storage/crawl-cookies/lazada/$this->countryId/");
        $jsid = md5($cookiesPath);
        $params = array('TPL_username' => $username, 'TPL_password' => $password, 'has_captcha' => 0);
        $strParam = '';
        foreach ($params as $k => $p) {
            $strParam = $strParam . $k . '=' . urlencode($p) . '&';
        }
        curl_setopt($ch, CURLOPT_URL, "$lazadaScUrl/seller/login/submit");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $strParam);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        $headers[] = "Cookie: c_csrf=fb8241d4-f22d-4717-8c36-af8fc25560cc; JSID=$jsid;";
        $headers[] = "Origin: $lazadaScUrl";
        $headers[] = "X-Xsrf-Token: fb8241d4-f22d-4717-8c36-af8fc25560cc";
        $headers[] = "Referer: $lazadaScUrl/seller/login";
        $headers[] = "Authority: $lazadaScDomain";
        $headers[] = "X-Requested-With: XMLHttpRequest";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);
        $url = $data['sessionUrl'] . '?token=' . $data['token'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookiesPath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookiesPath);
        $headers = array();
        $headers[] = "Cookie: c_csrf=fb8241d4-f22d-4717-8c36-af8fc25560cc; JSID=$jsid;";
        $headers[] = "Referer: $lazadaScUrl/seller/login";
        $headers[] = "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);
        $this->cookiesPath = $cookiesPath;
        return !(json_decode($result, true)['code'] ?? 1);
    }


    public function getProductList($page = 1, $limit = 50, $tab = 'all')
    {
        $params = [
            'title' => null,
            'sellerSku' => null,
            'sellerName' => null,
            'shopSku' => null,
            'itemId' => null,
            'brandName' => null,
            'selectCategory' => null,
            'orderBy' => null,
            'desc' => null,
            'currentTab' => $tab,
            'size' => $limit,
            'currentPage' => $page,
        ];
        $cookiesPath = $this->cookiesPath;
        $cookie = file_get_contents($cookiesPath);
        preg_match('/TID(.*?)(\n)/', $cookie, $TIDArr);
        preg_match('/CSRFT(.*?)(\n)/', $cookie, $CSRFTArr);
        $JSID = trim(md5($cookiesPath));
        $TID = trim($TIDArr[1]);
        $CSRFT = trim($CSRFTArr[1]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$this->lazadaSellerCenterUrl()}/product/portal/search?" . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        $headers[] = "Cookie: JSID=$JSID; CSRFT=$CSRFT; TID=$TID;";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }


    public function getOrderProductReview($from, $to, $page = 1, $limit = 50, $repliedState = null)
    {
        $params = [
            'pageSize' => $limit,
            'pageNo' => $page,
            'contentType' => null,
            'productName' => null,
            'orderNumber' => null,
            'sellerSku' => null,
            'shopSku' => null,
            'fromDate' => $from,
            'toDate' => $to,
            'rating' => null,
            'sourceAppName' => null,
            'replyState' => $repliedState,
            'isExternal' => false,
        ];
        $cookiesPath = $this->cookiesPath;
        $cookie = file_get_contents($cookiesPath);
        preg_match('/TID(.*?)(\n)/', $cookie, $TIDArr);
        preg_match('/CSRFT(.*?)(\n)/', $cookie, $CSRFTArr);
        $JSID = trim(md5($cookiesPath));
        $TID = trim($TIDArr[1]);
        $CSRFT = trim($CSRFTArr[1]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "{$this->lazadaSellerCenterUrl()}/asc-review/product-reviews/getList?" . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        $headers[] = "Accept: application/json;";
        $headers[] = "Cookie: JSID=$JSID; CSRFT=$CSRFT; TID=$TID;";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }

    public function getSellerPolicy()
    {
        $lazadaScUrl = $this->lazadaSellerCenterUrl();
        $cookiesPath = $this->cookiesPath;
        $cookie = file_get_contents($cookiesPath);
        preg_match('/TID(.*?)(\n)/', $cookie, $TIDArr);
        preg_match('/CSRFT(.*?)(\n)/', $cookie, $CSRFTArr);
        $JSID = trim(md5($cookiesPath));
        $TID = trim($TIDArr[1]);
        $CSRFT = trim($CSRFTArr[1]);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$lazadaScUrl/dashboard/seller_policy");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        $headers = array();
        $headers[] = "Cookie: JSID=$JSID; CSRFT=$CSRFT; TID=$TID;";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        #curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
    }

    # Environment Group -----------------------------------------------------------------------------------------------#

    public function lazadaSellerCenterUrl()
    {
        $result = '';
        switch ($this->countryId) {
            case Constants\Country::VIETNAM:
                $result = 'https://sellercenter.lazada.vn';
                break;
            case Constants\Country::PHILIPPINES:
                $result = 'https://sellercenter.lazada.com.ph';
                break;
            case Constants\Country::MALAYSIA:
                $result = 'https://sellercenter.lazada.com.my';
                break;
            case Constants\Country::SINGAPORE:
                $result = 'https://sellercenter.lazada.sg';
                break;
            case Constants\Country::INDONESIA:
                $result = 'https://sellercenter.lazada.co.id';
                break;
            case Constants\Country::THAILAND:
                $result = 'https://sellercenter.lazada.co.th';
                break;
        }
        return $result;
    }

    public function getLocationLazadaSellerCenter()
    {
        $result = '';
        switch ($this->countryId) {
            case Constants\Country::VIETNAM:
                $result = 'VN';
                break;
            case Constants\Country::PHILIPPINES:
                $result = 'PH';
                break;
            case Constants\Country::MALAYSIA:
                $result = 'MY';
                break;
            case Constants\Country::SINGAPORE:
                $result = 'SG';
                break;
            case Constants\Country::INDONESIA:
                $result = 'ID';
                break;
            case Constants\Country::THAILAND:
                $result = 'TH';
                break;
        }
        return $result;
    }

    private function lazadaMyDomain()
    {
        $result = '';
        switch ($this->countryId) {
            case Constants\Country::VIETNAM:
                $result = 'https://my.lazada.vn';
                break;
            case Constants\Country::PHILIPPINES:
                $result = 'https://my.lazada.com.ph';
                break;
            case Constants\Country::MALAYSIA:
                $result = 'https://my.lazada.com.my';
                break;
            case Constants\Country::SINGAPORE:
                $result = 'https://my.lazada.sg';
                break;
            case Constants\Country::INDONESIA:
                $result = 'https://my.lazada.co.id';
                break;
            case Constants\Country::THAILAND:
                $result = 'https://my.lazada.co.th';
                break;
        }
        return $result;
    }

    private function lazadaPublicDomain()
    {
        $result = '';
        switch ($this->countryId) {
            case Constants\Country::VIETNAM:
                $result = 'https://www.lazada.vn';
                break;
            case Constants\Country::PHILIPPINES:
                $result = 'https://www.lazada.com.ph';
                break;
            case Constants\Country::MALAYSIA:
                $result = 'https://www.lazada.com.my';
                break;
            case Constants\Country::SINGAPORE:
                $result = 'https://www.lazada.sg';
                break;
            case Constants\Country::INDONESIA:
                $result = 'https://www.lazada.co.id';
                break;
            case Constants\Country::THAILAND:
                $result = 'https://www.lazada.co.th';
                break;
        }
        return $result;
    }
    #------------------------------------------------------------------------------------------------------------------#

    # Constants -------------------------------------------------------------------------------------------------------#
    const CONTENT_SCORE = [
        'Fill mandatory attributes' => 1,
        'Fill attributes in Key Product Information' => 2,
        'Add min. 3 images for each variation' => 3,
        'Add >40 words in Long Description' => 4,
        'Add >1 image in Long Description' => 5,
    ];

    const CONTENT_SCORE_CRITERIA_1 = 'Fill mandatory attributes';
    const CONTENT_SCORE_CRITERIA_2 = 'Fill attributes in Key Product Information';
    const CONTENT_SCORE_CRITERIA_3 = 'Add min. 3 images for each variation';
    const CONTENT_SCORE_CRITERIA_4 = 'Add >40 words in Long Description';
    const CONTENT_SCORE_CRITERIA_5 = 'Add >1 image in Long Description';

    #------------------------------------------------------------------------------------------------------------------#
}