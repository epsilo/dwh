<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 04/10/2019
 * Time: 13:56
 */

namespace App\Library\Application;


use Illuminate\Database\Eloquent\Builder;

class Helper
{
    /**
     * Combines SQL and its bindings
     *
     * @param Builder $query
     * @return string
     */
    public static function getEloquentSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}