<?php /** @noinspection PhpDocMissingThrowsInspection */

namespace App\Library\Model;

use Illuminate\Database\Eloquent\Model;
use App\Library\Application\Functions\Convert;

class MongoModel
{
    //TODO: UNDER CONSTRUCTION | ĐANG XÂY DỰNG XIN ĐỪNG SỬ DỤNG HOẶC PHÁ HOẠI

    /**
     * Attributes
     * @var array
     */
    protected $attributes = [];

    static protected $connection;

    static protected $collectionName;

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->attributes[$name];
    }

    /**
     * @throws \Exception
     */
    public function save()
    {
        $data = $this->attributes;
        unset($data['_id']);
        unset($data->_id);
        if ($this->attributes['_id'] ?? null) {
            Mongodb::connection(static::$connection)->update(static::$collectionName, ['_id' => $this->attributes['_id']], $data, ['upsert' => true]);
        } else {
            Mongodb::connection(static::$connection)->insert(static::$collectionName, $data);
        }
    }

    public function setRawAttributes($attributes)
    {
        $this->attributes = $attributes;
    }

    public function __construct($attributes = [])
    {
        $this->attributes = $attributes;
    }

    /** @noinspection PhpDocMissingThrowsInspection */
    /**
     * @param $id
     * @return \MongoDB\Driver\Cursor
     */
    public static function getById($id)
    {
        return Mongodb::connection(static::$connection)->select(static::$collectionName, ['_id' => $id]);
    }

    /**
     * @param $document
     * @return \MongoDB\Driver\WriteResult
     */
    public static function insert($document)
    {
        return Mongodb::connection(static::$connection)->insert(static::$collectionName, $document);
    }

    /**
     * @param $filter
     * @param $data
     * @param $option ['multi' => false, 'upsert' => false]
     * @param $method
     * @return \MongoDB\Driver\WriteResult
     */
    public static function update($filter, $data, $option = [], $method = '$set')
    {
        return Mongodb::connection(static::$connection)->update(static::$collectionName, $filter, $data, $option, $method);
    }

    /**
     * @param $filter
     * @param $column
     * @param $options
     * @return \MongoDB\Driver\Cursor
     */
    public static function select($filter, $column = [], $options = [])
    {
        $data = Mongodb::connection(static::$connection)->select(static::$collectionName, $filter, $column, $options);
        /*
        $data = array_map(function ($item) {
            return new static($item);
        }, $data);
        */
        return $data;
    }

    /**
     * @param $filter
     * @return mixed
     */
    public function count($filter)
    {
        return Mongodb::connection(static::$connection)->count(static::$collectionName, $filter);
    }

    /**
     * @param $filter
     * @param $columnMax
     * @return mixed
     */
    public function max($filter, $columnMax)
    {
        return Mongodb::connection(static::$connection)->max(static::$collectionName, $filter, $columnMax);
    }

    /**
     * @return \MongoDB\Driver\Manager
     */
    public function getManager()
    {
        return Mongodb::connection(static::$connection)->getManager();
    }

    /**
     * Insert multi rows
     * @param array $documentArray
     * @return \MongoDB\Driver\WriteResult
     */
    public function batchInsert($documentArray)
    {
        return Mongodb::connection(static::$connection)->batchInsert(static::$collectionName, $documentArray);

    }

    /**
     * @param $pipeline
     * @return mixed
     */
    public static function aggregate($pipeline)
    {
        return Mongodb::connection(static::$connection)->aggregate(static::$collectionName, $pipeline);
    }

    /**
     * @param $pipeline
     * @return mixed
     */
    public static function watch($pipeline)
    {
        return Mongodb::connection(static::$connection)->watch(static::$collectionName, $pipeline);
    }

    public function toArray()
    {
        return Convert::stdClassToArray($this->attributes);
    }
}
