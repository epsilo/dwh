<?php
namespace App\Library\Model\ES\StagingArea;
use App\Library\Model\ElasticSearchModel;

class KernelSaleorderitemShopeeApi extends ElasticSearchModel
{
    static protected $connection = 'staging_area';

    static protected $indexName = 'kernel_saleorderitem_shopee_api';
}