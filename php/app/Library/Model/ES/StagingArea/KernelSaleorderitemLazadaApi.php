<?php
namespace App\Library\Model\ES\StagingArea;
use App\Library\Model\ElasticSearchModel;

class KernelSaleorderitemLazadaApi extends ElasticSearchModel
{
    static protected $connection = 'staging_area';

    static protected $indexName = 'kernel_saleorderitem_lazada_api';
}