<?php
namespace App\Library\Model\ES\StagingArea;
use App\Library\Model\ElasticSearchModel;

class KernelProductLazadaApi extends ElasticSearchModel
{
    static protected $connection = 'staging_area';

    static protected $indexName = 'kernel_product_lazada_api';
}