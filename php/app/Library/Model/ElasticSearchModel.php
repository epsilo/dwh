<?php


namespace App\Library\Model;


use Elasticsearch\ClientBuilder;
use Exception;

class ElasticSearchModel
{
    /**
     * Attributes
     * @var array
     */
    protected $attributes = [];

    static protected $connection;

    static protected $indexName;

    public static function insert($data, $type = 'doc')
    {
        return ElasticSearch::connection(static::$connection)->insert(static::$indexName, $data, $type);
    }

    public static function update($id, $data, $type = 'doc')
    {
        return ElasticSearch::connection(static::$connection)->update(static::$indexName, $id, $data, $type);
    }

    public static function get($id, $type = 'doc')
    {
        return ElasticSearch::connection(static::$connection)->get(static::$indexName, $id, $type);
    }

    public function search($inputParams, $verbose = false)
    {
        return ElasticSearch::connection(static::$connection)->search(static::$indexName, $inputParams, $verbose);
    }

    public function scroll($scrollId, $scrollTime)
    {
        return ElasticSearch::connection(static::$connection)->scroll($scrollId, $scrollTime);
    }
}