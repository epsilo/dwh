<?php

namespace App\Library\Model;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SqlModel extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return $this
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getDetail($id)
    {
        $data = self::find($id);
        return $data ? $data->toArray() : null;
    }

    /**
     * @return static[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    public static function insert($inputs)
    {
        $model = new static();
        foreach ($inputs as $key => $val) {
            $model->$key = $val;
        }
        return $model->save();
    }

    /**
     * @param int|array $id
     * @param array $params
     * @return bool
     */
    public static function updateData($id, $params)
    {
        $model = self::getById($id);
        foreach ($params as $key => $val) {
            $model->$key = $val;
        }
        return $model->save();
    }

    public static function deleteById($id)
    {
        $model = self::destroy($id);
        return $model;
    }

    /**
     * @param $input
     * @return mixed
     */
    public static function raw($input)
    {
        return DB::raw($input);
    }

    public static function insertBatch($batchData)
    {
        if (!(static::_tableName ?? null)) return 0;
        if (!$batchData) return 0;
        $batch = [];
        $header = array_keys(current($batchData));
        $baseQ = sprintf('insert into %s (%s) values ', static::_tableName, implode(',', $header));
        $count = 0;
        try {
            $pdo = DB::connection(static::CONNECTION_NAME)->getPdo();
            foreach ($batchData as $insertItem) {
                $formatItem = [];
                foreach ($header as $key) {
                    $formatItem[$key] = $pdo->quote($insertItem[$key] ?? null);
                }
                $batch[] = '(' . implode(',', $formatItem) . ')';
                if (count($batch) >= self::$configBatch) {
                    $count += $pdo->exec(sprintf('%s %s;', $baseQ, implode(',', $batch)));
                    $batch = [];
                }
            }
            if ($batch) {
                $count += $pdo->exec(sprintf('%s %s;', $baseQ, implode(',', $batch)));
            }
        } catch (\Exception $exception) {
            $count = 0;
        }
        return $count;
    }

    public static function updateBatch($batchData)
    {
        $count = 0;
        try {
            $batchData = array_values($batchData);
            $total = count($batchData);
            $j = 0;
            $i = 0;
            do {
                $j = min($total -1, $j + self::$configBatch);
                DB::connection(static::CONNECTION_NAME)->transaction(function() use ($batchData, &$count, $total, $j, &$i) {
                    for (; $i <= $j; $i++) {
                        $batchData[$i]->save();
                        $count++;
                    }
                });
            } while ($i < $total);
        } catch (\Exception $exception) {
            print_r($exception->getMessage());
            $count = 0;
        }
        return $count;
    }

    public static function updateFilter($data, $filter)
    {
        try {
            $queryString = 'update ' . static::_tableName.' set %s where '. str_replace('%','%%', $filter);
            $bindings = [];
            $queryStringSet = [];
            foreach ($data as $key => $value) {
                $queryStringSet[] = $key .'=?';
                $bindings[] = $value;
            }
            $queryString = sprintf($queryString, implode(',', $queryStringSet));
            $count = DB::connection(static::CONNECTION_NAME)->update($queryString, $bindings);
        } catch (\Exception $exception) {
            print_r($exception->getMessage());
            $count = 0;
        }
        return $count;
    }


    protected static $configBatch = 1000;
}
