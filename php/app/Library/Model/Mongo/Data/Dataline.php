<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 10/12/2019
 * Time: 09:34
 */
namespace App\Library\Model\Mongo\Data;

use App\Library\Model;

class Dataline extends Model\MongoModel
{
    static protected $connection = 'data';

    static protected $collectionName = 'dataline';
}