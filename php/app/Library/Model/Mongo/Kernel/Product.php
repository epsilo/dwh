<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 07/10/2019
 * Time: 14:40
 */
namespace App\Library\Model\Mongo\Kernel;
use App\Library\Model;

class Product extends Model\MongoModel
{
    static protected $connection = 'kernel';

    static protected $collectionName = 'product';
}