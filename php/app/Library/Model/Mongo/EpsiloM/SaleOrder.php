<?php
/**
 * Created by PhpStorm.
 * User: haou
 * Date: 07/10/2019
 * Time: 14:40
 */
namespace App\Library\Model\Mongo\EpsiloM;
use App\Library\Model;

class SaleOrder extends Model\MongoModel
{
    static protected $connection = 'epsilo_m';

    static protected $collectionName = 'sale_order';
}