<?php


namespace App\Library\Model;


use Elasticsearch\ClientBuilder;
use Exception;

class ElasticSearch
{
    private static $configs;
    private static $connections;

    private $client;

    /**
     * ElasticSearch constructor.
     * @param $connectionName
     * @throws Exception
     */
    private function __construct($connectionName)
    {
        $config = self::getConfig($connectionName);
        $host = $config['host'] ?? null;
        $port = $config['port'] ?? null;
        $scheme = $config['scheme'] ?? null;
        $host = [
            'host' => $host,
            'port' => $port,
            'scheme' => $scheme,
        ];
        $cluster = [$host];
        $this->client = ClientBuilder::create()->setHosts($cluster)->build();
    }


    /**
     * @param null $connectionName
     * @return static
     * @throws Exception
     */
    public static function connection($connectionName = null) {
        $connectionName = $connectionName ?? env('ES_DEFAULT_INDEX', '.kibana');
        return self::$connections[$connectionName] ?? self::initConnection($connectionName);
    }

    /**
     * @param $connectionName
     * @return static
     * @throws Exception
     */
    private static function initConnection($connectionName)
    {
        self::$connections[$connectionName] = new static($connectionName);
        return self::$connections[$connectionName];
    }

    /**
     * @param string $connectionName
     * @return mixed
     * @throws Exception
     */
    public static function getConfig($connectionName)
    {
        if (!self::$configs) {
            self::$configs = config('database.elastic_search');
        }
        if (!isset(self::$configs[$connectionName])) {
            throw new Exception("Undefined Elastic Search connection: $connectionName");
        }
        return self::$configs[$connectionName];
    }

    public function insert($index, $data, $type = 'doc')
    {
        $params = [
            'index' => $index,
            'type' => $type,
        ];
        if ($data['id'] ?? null) {
            $params['id'] = $data['id'];
            unset($data['id']);
        }
        $params['body'] = $data;
        return $this->client->index($params);
    }

    public function update($index, $id, $data, $type = 'doc')
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id,
            'body' => [
                'doc' => $data
            ]
        ];
        return $this->client->update($params);
    }

    public function get($index, $id, $type = 'doc')
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id,
        ];
        return $this->client->get($params);
    }

    public function search($index, $inputParams, $verbose = false)
    {
        $params = ['index' => $index];
        $params = array_merge($params, $inputParams);
        if ($verbose) {
            print_r($params);
        }
        $results = $this->client->search($params);
        return $results;
    }

    public function scroll($scrollId, $scrollTime)
    {
        $params = [
            'scroll_id' => $scrollId,
            'scroll' => $scrollTime
        ];
        $results = $this->client->scroll($params);
        return $results;
    }
}