<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelExample extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pep_support_center';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'pep_support_center_id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param int|array $id
     * @return mixed
     */
    public static function getById($id)
    {
        return self::find($id);
    }

    /**
     * @return ModelExample[]|\Illuminate\Database\Eloquent\Collection
     */
    public static function getAll()
    {
        return self::all();
    }

    public static function insert($inputs)
    {
        //Example
        //        $inputs = [
        //            'fk_organization'=>val,
        //            'fk_venture'=>val,
        //            'fk_pep_support_center_status'=>val,
        //            'fk_navigation'=>val,
        //            'pep_support_center_type'=>val,
        //            'pep_support_center_summary'=>val,
        //            'pep_support_center_skype'=>val,
        //            'pep_support_center_phone'=>val,
        //            'pep_support_center_created_at'=>val,
        //            'pep_support_center_created_by'=>val,
        //            'pep_support_center_updated_at'=>val,
        //            'pep_support_center_updated_by'=>val,
        //            'pep_support_center_closed_at'=>val,
        //            'pep_support_center_product'=>val,
        //            'pep_support_center_weight'=>val
        //        ]

        $model = new ModelExample();
        foreach ($inputs as $key => $val) {
            $model->$key = $val;
        }
        return $model->save();
    }


    /**
     * @param int|array $id
     * @param array $params
     * @return bool
     */
    public static function updateData($id, $params)
    {
        $model = self::find($id);
        foreach ($params as $key => $val) {
            $model->$key = $val;
        }
        return $model->save();
    }

    /**
     * @return mixed
     */
    public static function queryBuild()
    {
        $model = DB::connection('master')->table('user')
            ->join('config_active', 'user.fk_config_active', '=', 'config_active.config_active_id')
            ->select('user.*', 'config_active.*')
            ->where('user_id', 1)
            ->get();
        return $model;
    }
    public static function deleteById($id)
    {
        $model = self::where('pep_support_center_id',$id)->delete();
        return $model;
    }


}