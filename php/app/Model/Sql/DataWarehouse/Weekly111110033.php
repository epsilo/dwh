<?php

namespace App\Model\Sql\DataWarehouse;

class Weekly111110033  extends \App\Library\Model\Sql\DataWarehouse\Weekly111110033
{
    public static function getByKey($shopId, $time)
    {
        $query = self::query()
            ->from(self::_tableName)
            ->where(self::COL_SHOP_ID, intval($shopId))
            ->where(self::COL_TIME, trim($time));
        return $query->first();
    }
}