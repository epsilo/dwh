<?php
namespace App\Model\Sql\Kernel;
class Checker111110033 extends \App\Library\Model\Sql\Kernel\Checker111110033
{
    public static function getByKeyIndex($shopId, $channelId, $identify)
    {
        $query = self::query()
            ->from(self::_tableName)
            ->where(self::COL_SHOP_ID, intval($shopId))
            ->where(self::COL_CHANNEL_ID, intval($channelId))
            ->where(self::COL_IDENTIFY, trim($identify));
        return $query->first();
    }
}