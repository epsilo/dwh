<?php
/**
 * Created by PhpStorm.
 * User: nhan
 * Date: 14/09/2019
 * Time: 17:01
 */

namespace App\Mail;

use Illuminate\Mail\Mailable;


class TestSendMail extends Mailable
{

    public function __construct()
    {

    }

    /**
     * @return $this
     */
    public function build()
    {
        $now = date('Y-m-d H:i:s');

        return $this->view('email.test')
            ->subject("Title at $now")
            ->with([
                'name' => "Cin Nhan $now",
                'body' => "Body $now"
            ])
        ;
    }
}